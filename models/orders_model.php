<?php

class Orders_model extends Abstraction_model {

	public $abstraction_database   = "overtimeink";
	public $abstraction_table      = "orders";
	public $abstraction_key        = "id";
	public $abstraction_savable    = true;
	public $abstraction_deletable  = true;
	public $abstraction_search     = true;
	public $abstraction_fields     = array(
		"id"              => "key",
		"user_id"         => "Users_model",
		"address"         => "string",
		"city"            => "string",
		"state"           => "enum",
		"postcode"        => "string",
		"customer_notes"  => "text",
		"internal_notes"  => "text",
		"status"          => "enum",
		"options"         => "set",
		"start"           => "datetime",
		"end"             => "datetime",
		"created"         => "datetime",
		"updated"         => "datetime",
		"shipped"         => "datetime",
	);
	public $abstraction_required   = array(
	);

	public function counts() {

		// Instantiate
		$database  = new Database();

		// Clear
		$database->execute("

			SELECT
				COUNT(*)                                              AS `all_count`,
				SUM(CASE WHEN `status` != 'shipped' THEN 1 ELSE 0 END)   `open_count`,
				SUM(CASE WHEN `status` = 'ordered' THEN 1 ELSE 0 END)    `ordered_count`,
				SUM(CASE WHEN `status` = 'supplied' THEN 1 ELSE 0 END)   `supplied_count`,
				SUM(CASE WHEN `status` = 'scheduled' THEN 1 ELSE 0 END)  `scheduled_count`,
				SUM(CASE WHEN `status` = 'completed' THEN 1 ELSE 0 END)  `completed_count`,
				SUM(CASE WHEN `status` = 'shipped' THEN 1 ELSE 0 END)    `shipped_count`
			FROM
				`?`.`?`

		", $this->abstraction_database, $this->abstraction_table);

		// Return
		return array(
			"all"        => $database->row("all_count"),
			"open"       => $database->row("open_count"),
			"ordered"    => $database->row("ordered_count"),
			"supplied"   => $database->row("supplied_count"),
			"scheduled"  => $database->row("scheduled_count"),
			"completed"  => $database->row("completed_count"),
			"shipped"    => $database->row("shipped_count"),
		);

	}

	public function review_email() {

		// Instantiate
		$frequency = new Frequency();
		$email     = new Email();
		$text_body = new View("emails/review-text");
		$html_body = new View("emails/review-html");

		// No ID?
		if (is_null($this->user_id->id)) return false;

		// Set HTML View
		$html_body->path     = $frequency->application->path->consumer_desktop;
		$html_body->id       = $this->id;
		$html_body->checksum = substr(hash("sha512", "review" . $frequency->application->seed . $this->id), 32, 10);

		// Set Text View
		$text_body->path     = $frequency->application->path->consumer_desktop;
		$text_body->id       = $this->id;
		$text_body->checksum = substr(hash("sha512", "review" . $frequency->application->seed . $this->id), 32, 10);

		// Set From
		$from = array(
			"name"  => $frequency->application->name,
			"email" => $frequency->application->reply,
		);

		// Set Recipient
		$recipient = array(array(
			"name"  => "{$this->user_id->firstname} {$this->user_id->lastname}",
			"email" => $this->user_id->email,
		));

		// Send Email
		$email->send($from, $recipient, "Review proof of concepts for Order #{$this->id}!", $text_body->export(), $html_body->export());

		// Success
		return true;

	}

	public function approve_shipping($date) {

		// Instantiate
		$frequency = new Frequency();
		$email     = new Email();
		$text_body = new View("emails/shipping-text");
		$html_body = new View("emails/shipping-html");

		// No ID?
		if (is_null($this->user_id->id)) return false;

		// Set HTML View
		$html_body->path = $frequency->application->path->consumer_desktop;
		$html_body->id   = $this->id;
		$html_body->date = $date;

		// Set Text View
		$text_body->path = $frequency->application->path->consumer_desktop;
		$text_body->id   = $this->id;
		$text_body->date = $date;

		// Set From
		$from = array(
			"name"  => $frequency->application->name,
			"email" => $frequency->application->reply,
		);

		// Set Recipient
		$recipient = array(array(
			"name"  => "{$this->user_id->firstname} {$this->user_id->lastname}",
			"email" => $this->user_id->email,
		));

		// Send Email
		$email->send($from, $recipient, "Your shipping date for Order #{$this->id}!", $text_body->export(), $html_body->export());

		// Success
		return true;

	}

}
