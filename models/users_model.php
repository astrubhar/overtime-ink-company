<?php

class Users_model extends Abstraction_model {

	public $abstraction_database   = "overtimeink";
	public $abstraction_table      = "users";
	public $abstraction_key        = "id";
	public $abstraction_savable    = true;
	public $abstraction_deletable  = true;
	public $abstraction_search     = true;
	public $abstraction_fields     = array(
		"id"          => "key",
		"email"       => "email",
		"password"    => "string",
		"firstname"   => "string",
		"lastname"    => "string",
		"company"     => "string",
		"phone"       => "number",
		"phone_ext"   => "number",
		"address"     => "string",
		"city"        => "string",
		"state"       => "enum",
		"postcode"    => "string",
		"notes"       => "text",
		"role"        => "enum",
		"options"     => "set",
		"created"     => "datetime",
		"updated"     => "datetime",
		"last_login"  => "datetime",
	);
	public $abstraction_required   = array(
		"firstname",
		"lastname",
		"role",
	);

	public function id_by_authentication($email, $password) {

		// Instantiate
		$frequency = new Frequency();
		$database  = new Database();

		// Hash Password
		$password = $this->generate_hash($email, $password);

		// Try to Match User
		$database->execute("

			SELECT
				`?`
			FROM
				`?`.`?`
			WHERE
				`email` = '?' AND
				`password` = '?'

		", $this->abstraction_key, $this->abstraction_database, $this->abstraction_table, $email, $password);

		// Return
		return ($database->total() > 0 ? $database->row($this->abstraction_key) : false);

	}

	public function generate_hash($email, $password) {

		// Return Cryptographic Hash
		return crypt($password, '$6$rounds=500000$' . substr(hash('sha512', $email), 18, 8) . substr(hash('sha512', $email), 31, 8) . '$');

	}

}
