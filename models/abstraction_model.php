<?php

class Abstraction_model {

	public $abstraction_keys = array();
	public $abstraction_enumerated_values = array();
	public $abstraction_order = array();

	public function __construct($key = "") {

		// Iterate Fields
		foreach ($this->abstraction_fields as $field => $type) {

			// Set Initial Value
			$this->$field = $this->typecast($type, $field, "");

		}

		// Read?
		if (!empty($key)) {

			// Read
			$this->read($key);

		}

	}

	public function __clone() {

		// Iterate Properties
		foreach ($this as $key => $value) {

			// Object?
			if (is_object($value)) {

				// Clone
				$this->$key = clone $this->$key;

			}

		}
	}

	public function __toString() {

		// Define
		$array = array();

		// Iterate
		foreach ($this as $key => $value) {

			// Filter by Type
			switch ($key) {

				case is_object($value):
					$array[$key] = $this->$key->__toString();
					break;

				case "abstraction_database":
				case "abstraction_table":
				case "abstraction_key":
				case "abstraction_savable":
				case "abstraction_deletable":
				case "abstraction_search":
				case "abstraction_fields":
				case "abstraction_required":
				case "abstraction_keys":
				case "abstraction_enumerated_values":
				case "abstraction_order":
					break;

				case is_bool($value):
					$array[$key] = ($value ? "TRUE" : "FALSE");
					break;

				case $this->abstraction_key:
					if (is_null($value)) {
						$array[$key] = "NULL (unsaved)";
					} else {
						$array[$key] = $value;
					}
					break;

				default:
					$array[$key] = $value;

			}

		}

		// Buffer
		ob_start();

		// Print
		print_r($array);

		// Return String
		return ob_get_clean();

	}

	public function toArray($array = array()) {

		// Iterate Properties
		foreach ($this as $key => $value) {

			// Skip Object Keys
			if (in_array($key, array(
				"abstraction_database",
				"abstraction_table",
				"abstraction_key",
				"abstraction_savable",
				"abstraction_deletable",
				"abstraction_search",
				"abstraction_fields",
				"abstraction_required",
				"abstraction_keys",
				"abstraction_enumerated_values",
				"abstraction_order",
			))) continue;

			// Object?
			elseif (is_object($value)) {

				// Clone
				$array[$key] = $this->$key->toArray();

			}

			// Field?
			else {

				$array[$key] = $value;

			}

		}

		// Return Array
		return $array;
	}

	public function read($key) {

		// Instantiate
		$database = new Database();

		// Fetch Components
		$selects = $this->selects();
		$joins = $this->joins();

		// Execute Query
		$database->execute("

			SELECT
				" . implode(",\n", $selects) . "
			FROM
				`?`.`?`
			" . implode("\n", $joins) . "
			WHERE
				`?`.`?`.`?` = '?'

		", $this->abstraction_database, $this->abstraction_table, $this->abstraction_database, $this->abstraction_table, $this->abstraction_key, $key);

		// Parse
		$this->parse($database->row());

	}

	public function find($current_page = 1, $phrase = "", $results_per_page = null, $conditions = array()) {

		// Instantiate
		$frequency = new frequency();
		$database = new Database();

		// Use default?
		if (is_null($results_per_page)) {

			$results_per_page = $frequency->application->results_per_page;

		}

		// Define
		$results = array();

		// Fetch Components
		$selects = $this->selects();
		$joins = $this->joins();

		// Expand Phrase
		$keywords = explode(" ", str_replace("+", " ", urldecode($phrase)));

		// Iterate Keywords
		foreach ($keywords as $keyword) {

			// Add Conditions for Keyword
			$conditions[] = "(" . implode(" OR\n", $this->conditions($keyword)) . ")";

		}

		// Calculate Offset
		$limit_offset = ($current_page - 1) * $results_per_page;

		// Enable Profiling
		$database->execute("SET profiling = 1");

		// Execute Query
		$database->execute("

			SELECT SQL_CALC_FOUND_ROWS
				" . implode(",\n", $selects) . "
			FROM
				`?`.`?`
			" . implode("\n", $joins) . "
			WHERE
				" . implode(" AND\n", $conditions) . "
			" . (count($this->abstraction_order) > 0 ? "ORDER BY " . implode(", ", $this->abstraction_order) : null) . "
			LIMIT
				?, ?

		", $this->abstraction_database, $this->abstraction_table, $limit_offset, $results_per_page);

		// Iterate Rows
		while ($row = $database->fetch()) {

			// Parse
			$this->parse($row);

			// Clone and Store in Results
			$results[] = clone $this;

		}

		// Get Total Results
		$database->execute("SELECT FOUND_ROWS() AS `count`");

		// Set
		$this->abstraction_count = (int) $database->row("count");
		$this->abstraction_pages = (int) ceil($this->abstraction_count / $results_per_page);

		// Get Query Execution Time
		$database->execute("SELECT SUM(`duration`) AS `duration` FROM `information_schema`.`profiling` GROUP BY `query_id` ORDER BY `query_id` DESC LIMIT 1");

		// Set
		$this->abstraction_duration = $database->row("duration");

		// Disable Profiling
		$database->execute("SET profiling = 0");

		// Return
		return $results;

	}

	public function order($table, $field, $direction = "desc") {

		// Set
		$this->abstraction_order[] = "`{$table}`.`{$field}` {$direction}";

	}

	public function pagination($page) {

		// Instantiate
		$frequency = new frequency();

		// Less than count?
		if ($this->abstraction_pages <= $frequency->application->pagination_items) {

			// Set
			$this->pagination_start = 1;
			$this->pagination_end   = $this->abstraction_pages;

		}

		// Exceeds count?
		else {

			// Set
			$this->pagination_start	= $page - floor($frequency->application->pagination_items / 2);
			$this->pagination_end   = $page + floor($frequency->application->pagination_items / 2);

			// Out-of-range?
			if ($this->pagination_start < 0) {

				// Adjust
				$this->pagination_end   += abs($this->pagination_start);
				$this->pagination_start  = 1;

			}

			// Out-of-range?
			elseif ($this->pagination_end > $this->abstraction_pages) {

				// Adjust
				$this->pagination_start -= $this->pagination_end - $this->abstraction_pages;
				$this->pagination_end    = $this->abstraction_pages;

			}

		}

	}

	public function save($errors = array()) {

		// Instantiate
		$database = new Database();

		// Create Buffer
		$database->execute("START TRANSACTION");

		// Clone Object
		$that = clone $this;

		// Write Tables
		$errors = $that->write($database, "", $errors);

		// Did all items save without error?
		if (empty($errors)) {

			// Save Buffer
			$database->execute("COMMIT");

			// Iterate Fields
			foreach ($that as $field => $value) {

				// Set
				$this->$field = $value;

			}

		} else {

			// Discard Buffer (Don't Save)
			$database->execute("ROLLBACK");

		}

		// Discard Changes
		unset($that);

		// Return
		return $errors;

	}

	public function delete() {

		// Delete this table?
		if ($this->abstraction_deletable === true) {

			// Instantiate
			$database = new Database();

			// Execute Query
			$status = $database->execute("

				DELETE FROM
					`?`.`?`
				WHERE
					`?`.`?`.`?` = '?'
				LIMIT 1

			", $this->abstraction_database, $this->abstraction_table, $this->abstraction_database, $this->abstraction_table, $this->abstraction_key, $this->{$this->abstraction_key});

			// Return Status
			return ($status > 0 ? true : false);

		} else {

			// Aborted
			return false;

		}


	}

	private function write($database, $namespace, $errors) {

		// Iterate Fields
		foreach ($this->abstraction_fields as $field => $type) {

			// Found class?
			if (is_object($this->$field)) {

				// Instruct Children to Save
				$errors = $this->$field->write($database, "{$namespace}{$field}.", $errors);

			}

		}

		// Save this table?
		if ($this->abstraction_savable === true) {

			// Validate
			$errors = array_merge($errors, $this->validate($namespace));

			// Fetch Statements
			$updates = $this->updates();

			// Execute Query
			$result = $database->execute("

				INSERT INTO
					`?`.`?`
				SET
					" . implode(",\n", $updates) . "
				ON DUPLICATE KEY UPDATE
					" . implode(",\n", $updates) . "

			", $this->abstraction_database, $this->abstraction_table);

			// Was row affected?
			if ($result) {

				// Has ID set?
				if (is_null($this->{$this->abstraction_key})) {

					// Set ID
					$this->{$this->abstraction_key} = $database->id();

				}

			}

			// Error while fetching results?
			else {

				// Add Table Error
				$errors[] = "table.{$this->abstraction_table}";

			}

		}

		// Return
		return $errors;

	}

	private function parse($row) {

		// Passed row?
		if (is_array($row) or is_object($row)) {

			// Typecast as Array
			$row = (array) $row;

			// Define
			$subobjects = array();

			// Iterate Fields
			foreach ($row as $key => $value) {

				// Sub-object?
				if (strstr($key, "*")) {

					// Expand Components
					$components = explode("*", $key);

					// Retrieve Namespace
					$namespace = array_shift($components);

					// Implode Components
					$key = implode("*", $components);

					// Add To Sub-objects
					$subobjects[$namespace][$key] = $value;

				}

				// Is field of this object?
				elseif (array_key_exists($key, $this->abstraction_fields)) {

					// Set Value
					$this->$key = $this->typecast($this->abstraction_fields[$key], $key, $value);

				}

			}

			// Iterate Sub-objects
			foreach ($subobjects as $key => $subobject) {

				// Found array?
				if (array_key_exists($key, $this->abstraction_keys)) {

					// Expand
					$key = $this->abstraction_keys[$key];

				}

				// Instruct Children to Parse
				$this->$key->parse($subobject);

			}

		}

	}

	private function selects($namespace = "", $selects = array()) {

		// Iterate Fields
		foreach ($this->abstraction_fields as $field => $type) {

			// Found class?
			if (is_object($this->$field)) {

				// Set Object
				$object = $this->$field;

				// Found array?
				if (is_array($type)) {

					// Expand
					list($class, $field) = $type;

				}

				// Fetch Selects
				$selects = $object->selects("{$namespace}{$field}*", $selects);

			} else {

				// Add Field
				$selects[] = "`{$namespace}{$this->abstraction_table}`.`{$field}` AS `{$namespace}{$field}`";

			}

		}

		// Return
		return $selects;

	}

	private function joins($namespace = "", $joins = array()) {

		// Iterate Fields
		foreach ($this->abstraction_fields as $field => $type) {

			// Found class?
			if (is_object($this->$field)) {

				// Set Object
				$object = $this->$field;

				// Found array?
				if (is_array($type)) {

					// Map Key
					$this->abstraction_keys[$type[1]] = $field;

					// Expand
					list($class, $field) = $type;

				}

				// Add Join
				$joins[] = "LEFT JOIN `{$object->abstraction_database}`.`{$object->abstraction_table}` AS `{$namespace}{$field}*{$object->abstraction_table}` ON `{$namespace}{$field}*{$object->abstraction_table}`.`{$object->abstraction_key}` = `{$this->abstraction_database}`.`{$namespace}{$this->abstraction_table}`.`{$field}`";

				// Fetch Selects
				$joins = $object->joins("{$namespace}{$field}*", $joins);

			}

		}

		// Return
		return $joins;

	}

	private function conditions($keyword, $namespace = "", $conditions = array()) {

		// Instantiate
		$database = new Database();

		// Iterate Fields
		foreach ($this->abstraction_fields as $field => $type) {

			// Found class?
			if (is_object($this->$field)) {

				// Set Object
				$object = $this->$field;

				// Found array?
				if (is_array($type)) {

					// Expand
					list($class, $field) = $type;

				}

				// Fetch Conditions
				$conditions = $object->conditions($keyword, "{$namespace}{$field}*", $conditions);

			} elseif ($this->abstraction_search) {

				// Typecast
				$typecasted = $this->typecast($type, $field, $keyword);

				// Tweak Values
				switch ($type) {

					case "set":
						$typecasted = implode(",", $typecasted);
						break;

					case "date":
						$typecasted = ($typecasted == "0000-00-00" ? "" : $typecasted);
						break;

					case "datetime":
						$typecasted = ($typecasted == "0000-00-00 00:00:00" ? "" : $typecasted);
						break;

				}

				// Legitimate value?
				if (!empty($typecasted) and $type != "key") {

					// Add Field
					$conditions[] = "`{$namespace}{$this->abstraction_table}`.`{$field}` LIKE '%" . $database->escape($typecasted) . "%'";

				}

			}

		}

		// No conditions?
		if (empty($conditions)) {

			// True
			$conditions[] = "1";

		}

		// Return
		return $conditions;

	}

	private function updates($updates = array()) {

		// Instantiate
		$database = new Database();

		// Iterate Fields
		foreach ($this->abstraction_fields as $field => $type) {

			// Found object?
			if (is_object($this->$field)) {

				// Typecast
				$typecasted = $this->typecast("key", $field, $this->$field->{$this->$field->abstraction_key});

			} else {

				// Typecast
				$typecasted = $this->typecast($type, $field, $this->$field);

			}

			// Typecast of 'set'?
			if ($type == "set") {

				$typecasted = implode(",", $typecasted);

			}

			// Is null?
			if (is_null($typecasted)) {

				// Add Field
				$updates[] = "`{$field}` = NULL";

			} else {

				// Add Field
				$updates[] = "`{$field}` = '" . $database->escape($typecasted) . "'";

			}


		}

		// Return
		return $updates;

	}

	private function typecast($type, $field, $value) {

		// Find Type
		switch ($type) {

			case "key":
				$value = (empty($value) ? null : $value);
				break;

			case "number":
				$value = preg_replace("/[^0-9]/", "", $value);

			case "integer":
				$value = (int) preg_replace("/[^0-9]/", "", $value);
				break;

			case "float":
				$value = (float) preg_replace("/[^0-9\.]/", "", $value);
				break;

			case "boolean":
				$value = (($value === true or $value === 1 or $value === "1") ? true : false);
				break;

			case "string":
				$value = (string) filter_var($value, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
				break;

			case "email":
				$value = (string) filter_var($value, FILTER_SANITIZE_EMAIL);
				break;

			case "base64":
				$value = (string) (preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $value) ? $value : "");
				break;

			case "text":
				$value = (string) filter_var($value, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
				break;

			case "enum":
				$value = (in_array($value, $this->enumerated_values($field)) ? $value : "");
				break;

			case "set":
				$values = (is_array($value) ? $value : explode(",", $value));
				foreach ($values as $key => $value) {
					if (!in_array($value, $this->enumerated_values($field))) {
						unset($values[$key]);
					}
				}
				$value = $values;
				break;

			case "date":
				$value = (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $value) ? $value : "0000-00-00");
				break;

			case "datetime":
				$value = (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $value) ? $value : "0000-00-00 00:00:00");
				break;

			case is_array($type):
				$type = $type[0];
				# Don't Break

			case class_exists($type):
				$value = new $type($value);
				break;

			default:
				throw new exception("Abstraction Class: Unrecognized field-type or class of '{$type}'.");

		}

		// Return
		return $value;

	}

	private function validate($namespace = "") {

		// Define
		$errors = array();

		// Iterate Required Fields
		foreach ($this->abstraction_required as $field) {

			// Set
			$type = $this->abstraction_fields[$field];

			// Find Type
			switch ($type) {

				case "key":
				case "number":
				case "integer":
				case "float":
				case "string":
				case "base64":
				case "text":
					if (empty($this->$field)) {
						$errors[] = "{$namespace}{$field}";
					}
					break;

				case "email":
					if (!filter_var($this->$field, FILTER_VALIDATE_EMAIL)) {
						$errors[] = "{$namespace}{$field}";
					}
					break;

				case "enum":
					if (empty($this->$field)) {
						$errors[] = "{$namespace}{$field}";
					} elseif (!in_array($this->$field, $this->enumerated_values($field))) {
						$errors[] = "{$namespace}{$field}";
					}
					break;

				case "set":
					if (empty($this->$field)) {
						$errors[] = "{$namespace}{$field}";
					} else {
						$values = explode(",", $this->$field);
						foreach ($values as $key => $value) {
							if (!in_array($value, $this->enumerated_values($field))) {
								$errors[] = "{$namespace}{$field}";
								break;
							}
						}
					}
					break;

				case "date":
					if ($this->$field == "0000-00-00") {
						$errors[] = "{$namespace}{$field}";
					}
					break;

				case "datetime":
					if ($this->$field == "0000-00-00 00:00:00") {
						$errors[] = "{$namespace}{$field}";
					}
					break;

				case is_array($type):
				case class_exists($type):
					if (!is_numeric($this->$field->{$this->$field->abstraction_key}) or is_null($this->$field->{$this->$field->abstraction_key})) {
						$errors[] = "{$namespace}{$field}";
					}
					break;

				default:
					throw new exception("Abstraction Class: Unrecognized field-type or class of '{$type}'.");

			}

		}

		// Return
		return $errors;

	}

	private function enumerated_values($field) {

		// Create Database
		$database = new Database();

		// Not cached?
		if (!array_key_exists($field, $this->abstraction_enumerated_values)) {

			// Retrieve Field Information
			$database->execute("SHOW COLUMNS FROM `{$this->abstraction_database}`.`{$this->abstraction_table}` WHERE `Field` = '?'", $field);

			// Set Type
			$type = $this->abstraction_fields[$field];

			// Extract Values
			$values = explode("','", substr($database->row("Type"), ($type == "set" ? 5 : 6), strlen($database->row("Type")) - ($type == "set" ? 7 : 8)));

			// Cache
			$this->abstraction_enumerated_values[$field] = $values;

		}

		// Return
		return $this->abstraction_enumerated_values[$field];

	}

}
