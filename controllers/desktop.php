<?php

class Desktop extends Foundation {

	function __construct() {

		// Call Parent (Foundation)
		parent::__construct();

		// Start Output Buffer
		ob_start();

	}

	function __destruct() {

		// Capture Output
		$output = ob_get_clean();

		// Instantiate
		$header     = new View("header");
		$footer     = new View("footer");
		$navigation = new View("navigation");

		// Set Path and Title
		$header->path  = $this->path;
		$header->title = (isset($this->title) ? $this->title : "");

		// Set Selected Navigation Item
		$navigation->selected = static::$selected_navigation;
		$navigation->find     = (isset($this->find) ? $this->find : null);

		// Render Views
		$header->render();
		$navigation->render();
		echo $output;
		$footer->render();

		// Send Output
		ob_flush();

	}

}