<?php

class Calendar_JSON extends Foundation {

	function events($status, $start, $end, $timezone) {

		// Decode Timezone
		$timezone = str_replace("-", "/", $timezone);

		// Define
		$events = array();
		$i      = 1;

		// Instantiate
		$orders   = new Orders_model();
		$items    = new Items_model();

		// Convert Timestamps
		$start = $start / 1000;
		$end   = $end / 1000;

		// Set Conditions
		$conditions = array(
			"`orders`.`start` IS NOT NULL",
			"`orders`.`end`   IS NOT NULL",
			"
				((
					`orders`.`end` >= FROM_UNIXTIME(" . mysql_real_escape_string($start) . ") AND
					`orders`.`end` <= FROM_UNIXTIME(" . mysql_real_escape_string($end) . ")
				) OR (
					`orders`.`start` <= FROM_UNIXTIME(" . mysql_real_escape_string($end) . ") AND
					`orders`.`start` >= FROM_UNIXTIME(" . mysql_real_escape_string($start) . ")
				))
			",
		);

		// All filter?
		if ($status != "all") {

			// Filter Status
			$conditions[] = "`orders`.`status` = '" . mysql_real_escape_string($status) . "'";

		}

		// Find Matches
		$results = $orders->find(1, "", 9999, $conditions);

		// Iterate
		foreach ($results as $result) {

			// Convert Dates to Timestamps
			$result->start = strtotime($result->start);
			$result->end   = strtotime($result->end);

			// Convert to Millisecond Timestamps
			$result->start = $result->start * 1000;
			$result->end   = $result->end * 1000;

			// Set Cart Items
			$result->items = $items->find(1, "", 9999, array(
				"`items`.`order_id` = '" . mysql_real_escape_string($result->id) . "'",
			));

			// Set Title
			$title = "<b>{$result->user_id->firstname} {$result->user_id->lastname} (#{$result->id})</b> <ul style=\"margin: 0px;\">";

			// Iterate Items
			foreach ($result->items as $item) {

				$title .= "<li>{$item->quantity} &times; {$item->garment}</li>";

			}

			// Add Event
			$events[] = array(
				"id"    => $result->id,
				"title" => trim($title, ", ") . "</ul>",
				"url"   => "",
				"class" => "event-color event-color{$i}",
				"start" => $result->start,
				"end"   => $result->end,
			);

			// Increment Color
			$i++;

			// Reset Color (Out of Range)
			if ($i > 28) $i = 1;

		}

		// Save and Return
		echo json_encode(array(
			"success"    => 1,
			"result"     => $events,
		));

	}

}