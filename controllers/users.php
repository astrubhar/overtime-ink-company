<?php

class Users extends Desktop {

	static $selected_navigation = "users";

	public function index($country = "any", $state = "any", $role = "any", $page = 1, $find = "") {

		// Search?
		if (isset($_POST["find"])) {

			// Redirect
			$this->redirect("users/index/{$country}/{$state}/{$role}/1/" . urlencode($_POST["find"]));

		}

		// Instantiate
		$frequency    = new Frequency();
		$timeago      = new TimeAgo($frequency->application->timezone);
		$users        = new Users_model();
		$view         = new View("users/index");

		// URL Decode Find
		$find = str_replace("+", " ", urldecode($find));

		// Sort
		$users->order("users", "created", "desc");

		// Staff?
		if ($_SESSION["role"] != "administrator") {

			// Restrict Results
			$conditions = array(
				"`users`.`role` = 'customer'",
			);

		} else {

			// No Restrictions
			$conditions = array();

		}

		// Country?
		if ($country != "any") {

			// Filter
			$conditions[] = "`users`.`country` = '" . mysql_real_escape_string($country) . "'";

		}

		// Country?
		if ($state != "any") {

			// Filter
			$conditions[] = "`users`.`state` = '" . mysql_real_escape_string($state) . "'";

		}

		// Country?
		if ($role != "any") {

			// Filter
			$conditions[] = "`users`.`role` = '" . mysql_real_escape_string($role) . "'";

		}

		// Find Matches
		$view->users = $users->find($page, $find, $frequency->application->results_per_page, $conditions);

		// Calculate Pages
		$users->pagination($page);

		// Set Pagination
		$view->country   = $country;
		$view->state     = $state;
		$view->role      = $role;
		$view->pages     = $users->abstraction_pages;
		$view->page      = $page;
		$view->start     = $users->pagination_start;
		$view->end       = $users->pagination_end;
		$view->find      = $find;
		$view->timeago   = $timeago;
		$view->highlight = (!empty($find) ? str_replace(" ", ",", $find) : "");

		// Render
		$view->render();

		// Set Title and Search Term
		$this->title = "Users (" . count($view->users) . " of {$users->abstraction_count})";
		$this->find  = $find;

	}

}