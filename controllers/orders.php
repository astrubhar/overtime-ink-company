<?php

class Orders extends Desktop {

	static $selected_navigation = "orders";

	public function index($status = "all", $start = "+", $end = "+", $page = 1, $find = "") {

		// Search?
		if (isset($_POST["find"])) {

			// Redirect
			$this->redirect("orders/index/{$status}/{$start}/{$end}/1/" . urlencode($_POST["find"]));

		}

		// Instantiate
		$frequency    = new Frequency();
		$timeago      = new TimeAgo($frequency->application->timezone);
		$orders       = new Orders_model();
		$view         = new View("orders/index");

		// URL Decode Find
		$find = str_replace("+", " ", urldecode($find));

		// Set Default Start and End
		if (empty($start)) $start = "+";
		if (empty($end))   $end   = "+";

		// Sort
		$orders->order("orders", "created", "desc");

		// Define
		$conditions = array();

		// Has status?
		if ($status != "all") {

			// Filter
			$conditions[] = "`orders`.`status` = '" . mysql_real_escape_string($status) . "'";

		}

		// Has start and end date?
		if ($start != "+" and $end != "+") {

			// Filter
			$conditions[] = "`orders`.`created` >= '" . mysql_real_escape_string($start) . "'";
			$conditions[] = "`orders`.`created` <= '" . mysql_real_escape_string($end) . "'";

		}

		// Find Matches
		$view->orders = $orders->find($page, $find, $frequency->application->results_per_page, $conditions);

		// Calculate Pages
		$orders->pagination($page);

		// Set Pagination
		$view->status         = $status;
		$view->filter_start   = $start;
		$view->filter_end     = $end;
		$view->pages          = $orders->abstraction_pages;
		$view->total          = $orders->abstraction_count;
		$view->page           = $page;
		$view->start          = $orders->pagination_start;
		$view->end            = $orders->pagination_end;
		$view->counts         = $orders->counts();
		$view->find           = $find;
		$view->timeago        = $timeago;
		$view->highlight      = (!empty($find) ? str_replace(" ", ",", $find) : "");

		// Render
		$view->render();

		// Set Title and Search Term
		$this->title = "Orders (" . count($view->orders) . " of {$orders->abstraction_count})";
		$this->find  = $find;

	}

}