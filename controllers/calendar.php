<?php

class Calendar extends Desktop {

	static $selected_navigation = "calendar";

	public function index($status = "all", $page = 1, $find = "") {

		// Search?
		if (isset($_POST["find"])) {

			// Redirect
			$this->redirect("calendar/index/{$status}/1/" . urlencode($_POST["find"]));

		}

		// Instantiate
		$frequency    = new Frequency();
		$timeago      = new TimeAgo($frequency->application->timezone);
		$orders       = new Orders_model();
		$items        = new Items_model();
		$view         = new View("calendar/index");

		// URL Decode Find
		$find = str_replace("+", " ", urldecode($find));

		// Sort
		$orders->order("orders", "created", "desc");

		// Define
		$conditions = array();

		// Has status?
		if ($status != "all") {

			// Filter
			$conditions[] = "`orders`.`status` = '" . mysql_real_escape_string($status) . "'";

		} else {

			// Filter Shipped (Completed) Orders
			$conditions[] = "`orders`.`status` != 'shipped'";

		}

		// Find Matches
		$matched_orders = $orders->find($page, $find, 5, $conditions);

		// Iterate
		foreach ($matched_orders as &$matched_order) {

			// Set Cart Items
			$matched_order->items = $items->find(1, "", 9999, array(
				"`items`.`order_id` = '" . mysql_real_escape_string($matched_order->id) . "'",
			));

		}
		unset($matched_order);

		// Calculate Pages
		$orders->pagination($page);

		// Set Pagination
		$view->orders    = $matched_orders;
		$view->status    = $status;
		$view->pages     = $orders->abstraction_pages;
		$view->total     = $orders->abstraction_count;
		$view->page      = $page;
		$view->start     = $orders->pagination_start;
		$view->end       = $orders->pagination_end;
		$view->counts    = $orders->counts();
		$view->find      = $find;
		$view->timeago   = $timeago;
		$view->highlight = (!empty($find) ? str_replace(" ", ",", $find) : "");

		// Render
		$view->render();

		// Set Title and Search Term
		$this->title = "Calendar";
		$this->find  = $find;

	}

}