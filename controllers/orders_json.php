<?php

class Orders_JSON extends Foundation {

	function edit($id = null) {

		// Instantiate
		$order        = new Orders_model($id);
		$attachments  = new Attachments_model();
		$frequency    = new Frequency();
		$timeago      = new TimeAgo($frequency->application->timezone);

		// Format Phone Number
		$order->user_id->phone = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $order->user_id->phone);

		// Expand Country and State
		list($country, $state) = explode("-", $order->user_id->state);

		// Set Customer Input Label
		$order->user_id->label = "{$order->user_id->firstname} {$order->user_id->lastname}, {$order->user_id->address}, {$order->user_id->city}, {$state}, {$country}";
		$order->user_id->desc  = "<strong>{$order->user_id->firstname} {$order->user_id->lastname}</strong><br>{$order->user_id->address}<br>{$order->user_id->city}, {$state} {$order->user_id->postcode}, {$country}<br>" . preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $order->user_id->phone) . "<br>{$order->user_id->email}";

		// Remove Password
		unset($order->user_id->password);

		// Define
		$files = array();

		// Find Matches
		$matches = $attachments->find(1, "", 9999, array(
			"`attachments`.`order_id` = '" . mysql_real_escape_string($id) . "'",
		));

		// Iterate
		foreach ($matches as $match) {

			// Add
			$files[] = $match->toArray();

		}

		// Convert Dates
		$order->start                 = ($order->start == "0000-00-00 00:00:00" ? null : date("m/d/Y g:i A", strtotime($order->start)));
		$order->end                   = ($order->end == "0000-00-00 00:00:00" ? null : date("m/d/Y g:i A", strtotime($order->end)));
		$order->shipped               = ($order->shipped == "0000-00-00 00:00:00" ? null : $order->shipped);
		$order->created               = $timeago->inWords($order->created, "now");
		$order->updated               = $timeago->inWords($order->updated, "now");
		$order->user_id->last_login   = $timeago->inWords($order->user_id->last_login, "now");

		// Return
		echo json_encode(array(
			"order"          => $order->toArray(),
			"attachments"    => $files,
			"items"          => $this->edit_cart($id),
		));

	}

	private function edit_cart($id = null) {

		// Instantiate
		$items = new Items_model();

		// Define
		$response = array();

		// Find Matches
		$matches = $items->find(1, "", 9999, array(
			"`items`.`order_id` = '" . mysql_real_escape_string($id) . "'",
		));

		// Iterate Responses
		foreach ($matches as $match) {

			// Remove Order
			unset($match->order_id);

			// Set Response
			$response[] = $match->toArray();

		}

		// Return
		return $response;

	}

	function save($id = null) {

		// Instantiate
		$order = new Orders_model($id);

		// Read User
		$order->user_id->read($_POST["user_id"]);

		// Set Values
		$order->address           = $_POST["address"];
		$order->city              = $_POST["city"];
		$order->state             = $_POST["state"];
		$order->postcode          = $_POST["postcode"];
		$order->internal_notes    = $_POST["internal_notes"];
		$order->user_id->notes    = $_POST["customer_notes"];
		$order->shipped           = (empty($_POST["shipped"]) ? null : $_POST["shipped"]);
		$order->status            = (trim($_POST["status"]) == "" ? "ordered" : $_POST["status"]);

		// New?
		if (is_null($order->id)) {

			// Set Created Date
			$order->created = date("Y-m-d H:i:s");

		} else {

			// Set Updated Date
			$order->updated = date("Y-m-d H:i:s");

		}

		// Save
		$errors = $order->save();

		// No errors?
		if (empty($errors)) {

			// Iterate Attachments
			foreach ($_POST["attachments"] as $attachment_id) {

				// Instantiate
				$attachment = new Attachments_model($attachment_id);

				// Read Order ID
				$attachment->order_id = $order->id;

				// Save
				$attachment->save();

			}

		}

		// Save and Return
		echo json_encode(array(
			"id"     => $order->id,
			"errors" => $errors,
		));

	}

	function save_cart($order_id) {

		// Instantiate
		$item = new Items_model();

		// Set
		$item->quantity   = $_POST["quantity"];
		$item->decoration = $_POST["decoration"];
		$item->color      = $_POST["color"];
		$item->garment    = $_POST["garment"];
		$item->artwork    = $_POST["artwork"];

		// Read Order
		$item->order_id->read($order_id);

		// Save
		$item->save();

		// Save and Return
		echo json_encode(array(
			"errors" => $item->save(),
		));

	}

	function save_schedule($id = null) {

		// Instantiate
		$order = new Orders_model($id);

		// Set Values
		$order->start  = $_POST["start"];
		$order->end    = $_POST["end"];
		$order->status = $_POST["status"];

		// Save and Return
		echo json_encode(array(
			"errors" => $order->save(),
		));

	}

	function delete($id) {

		// Instantiate
		$order = new Orders_model($id);

		// Delete and Return
		echo json_encode(array(
			"status" => $order->delete(),
		));

	}

	function delete_cart($id) {

		// Instantiate
		$item = new Items_model($id);

		// Delete and Return
		echo json_encode(array(
			"status" => $item->delete(),
		));

	}

	function delete_upload($id) {

		// Instantiate
		$attachment = new Attachments_model($id);

		// Delete and Return
		echo json_encode(array(
			"status" => $attachment->delete(),
		));

	}

	function search_users() {

		// Instantiate
		$users = new Users_model();

		// Define
		$response = array();

		// Search
		$results = $users->find(1, $_POST["find"], 10, array(
			"`users`.`role` = 'customer'",
		));

		// Iterate
		foreach ($results as $row) {

			// Expand
			list($country, $state) = explode("-", $row->state);

			// Add
			$response[] = array(
				"id"    => $row->id,
				"label" => "{$row->firstname} {$row->lastname}, {$row->address}, {$row->city}, {$state}, {$country}",
				"desc"  => "<strong>{$row->firstname} {$row->lastname}</strong><br>{$row->address}<br>{$row->city}, {$state} {$row->postcode}, {$country}<br>" . preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $row->phone) . "<br>{$row->email}",
				"notes" => $row->notes,
			);

		}

		// Respond
		echo json_encode(array(
			"results" => $response,
		));

	}

	function get_address($id = null) {

		// Instantiate
		$user = new Users_model($id);

		// Return
		echo json_encode(array(
			"address"    => $user->address,
			"city"       => $user->city,
			"state"      => $user->state,
			"postcode"   => $user->postcode,
		));

	}

	function upload_file() {

		// Instantiate
		$attachments = new Attachments_model();

		// Attachment?
		if (isset($_FILES["file_attachment"])) {

			// Upload File
			$attachments->upload($_FILES["file_attachment"], "attachment");

		}

		// Proof of Concept?
		else {

			// Upload File
			$attachments->upload($_FILES["file_concept"], "concept");

		}


		// Return Result
		echo json_encode($attachments->toArray());

	}

	function request_review($id) {

		// Instantiate
		$order = new Orders_model($id);

		// Send Email
		$status = $order->review_email();

		// Save and Return
		echo json_encode(array(
			"status" => $status,
		));

	}

	function approve_shipping($id, $date) {

		// Instantiate
		$order = new Orders_model($id);

		// Decode
		$date = urldecode($date);

		// Send Email
		$status = $order->approve_shipping($date);

		// Save and Return
		echo json_encode(array(
			"status" => $status,
		));

	}

}