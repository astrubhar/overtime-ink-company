$(document).ready(function () {

	/**
	 *
	 *  Intervals
	 *
	**/

	// Every Minute
	setInterval(function () {

		// Refresh Page
		$("body").trigger("refresh");

	}, 60000);

	/**
	 *
	 *  Interaction Events
	 *
	**/

	// Listen for Key Presses
	$(document).on("keypress", "body", function (event) {

		// Is this an input control?
		if ($("input:focus,textarea:focus").length > 0) {

			// Ignore Keypress
			return;

		}

		// Parse Literal Character
		character = String.fromCharCode(event.which);

		// Is modal open?
		if ($("#modifyModal").hasClass("in")) {

			// Match Characters
			switch (character) {

				case "1":
					$(".stepwizard-step[data-value=ordered] button").trigger("click");
					event.preventDefault();
				break;

				case "2":
					$(".stepwizard-step[data-value=supplied] button").trigger("click");
					event.preventDefault();
				break;

				case "3":
					$(".stepwizard-step[data-value=scheduled] button").trigger("click");
					event.preventDefault();
				break;

				case "4":
					$(".stepwizard-step[data-value=completed] button").trigger("click");
					event.preventDefault();
				break;

				case "5":
					$(".stepwizard-step[data-value=shipped] button").trigger("click");
					event.preventDefault();
				break;

			}

		}

	});

	// Delete Button
	$(document).on("click", "[data-action=delete]", function (event) {

		// Show Modal
		$("#deleteModal").trigger("show", [{
			"id"    : $(this).attr("data-id"),
			"label" : $(this).attr("data-label")
		}]);

		// Cancel Event
		event.preventDefault();

	});

	// Confirm Delete Button
	$(document).on("click", "[data-action=confirm_delete]", function (event) {

		// Send Request
		$.ajax({
			url: "orders_json/delete/" + $("#deleteModal").find("input[name=id]").val(),
			dataType: "json",
			success: function(data) {

				// Successful?
				if (data.status == true) {

					// Refresh Page
					$("body").trigger("refresh");

					// Dismiss Modal
					$("#deleteModal").modal("hide");

				}

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	// Create Button
	$(document).on("click", "[data-action=create]", function (event) {

		// Show Modal
		$("#modifyModal").trigger("show", [{}]);

		// Cancel Event
		event.preventDefault();

	});

	// Modify Button
	$(document).on("click", "[data-action=modify]", function (event) {

		// Send Request
		$.ajax({
			url: "orders_json/edit/" + $(this).attr("data-id"),
			dataType: "json",
			success: function(response) {

				// Show Modal
				$("#modifyModal").trigger("show", [response.order, response.attachments, response.items]);

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	// View Cart Button
	$(document).on("click", "[data-action=view_cart]", function (event) {

		// Hide Modify
		$("#modifyModal").modal("hide");

		// Show Cart
		$("#cartModal").trigger("show");

		// Cancel Event
		event.preventDefault();

	});

	// Add Cart Item Button
	$(document).on("click", "[data-action=add_item]", function (event) {

		// Clear Errors
		$("#cart_list tfoot .form-group").removeClass("has-error");

		// Show Errors
		$("#cart_list tfoot .form-control").filter(function() { return this.value == ""; }).parents(".form-group").addClass("has-error");

		// Error free?
		if ($("#cart_list tfoot .form-group.has-error").length < 1) {

			// Add Row
			$("#cartModal").trigger("add_row", [{
				"id"            : "",
				"quantity"      : $("#cart_list #form_quantity").val(),
				"decoration"    : $("#cart_list #form_decoration").val(),
				"color"         : $("#cart_list #form_color").val(),
				"garment"       : $("#cart_list #form_garment").val(),
				"artwork"       : $("#cart_list #form_artwork").val()
			}]);

			// Clear Form
			$("#cart_list tfoot .form-control").val("");

		}

		// Cancel Event
		event.preventDefault();

	});

	// Remove Cart Item Button
	$(document).on("click", "[data-action=remove_item]", function (event) {

		// Copy
		that = this;

		// Confirm
		if (confirm("Are you sure?")) {

			// Set ID
			id = $(this).parents("tr").attr("data-id");

			// Has ID?
			if (id != "") {

				// Send Request
				$.ajax({
					url: "orders_json/delete_cart/" + id,
					dataType: "json",
					success: function(response) {

						// Remove Row
						$(that).parents("tr").remove();

					}
				});

			} else {

				// Remove Row
				$(that).parents("tr").remove();

			}

		}

		// Cancel Event
		event.preventDefault();

	});

	// Close Cart Button
	$(document).on("click", "[data-action=cart_return]", function (event) {

		// Hide Cart
		$("#cartModal").modal("hide");

		// Show Modify
		$("#modifyModal").modal("show");

		// Cancel Event
		event.preventDefault();

	});

	// Save Button
	$("[data-action=save]").on("click", function (event) {

		// Get Values
		values = $("#modifyModal").triggerHandler("get");

		// Send Request
		$.ajax({
			type: "POST",
			url: "orders_json/save/" + values.id,
			data: values,
			dataType: "json",
			success: function(data) {

				// Errors?
				if (data.errors.length > 0) {

					// Clear Errors
					$("#modifyModal").find(".form-group").removeClass("has-error");

					// Iterate Errors
					$.each(data.errors, function(index, value) {

						// Mark Error
						$("#modifyModal").find("#form_" + value).parents(".form-group").addClass("has-error");

					});

				}

				// Success?
				else {

					// Get Items
					items = $("#cartModal").triggerHandler("get");

					// Iterate Items
					$.each(items, function(index, value) {

						// Needs ID?
						if (value.id == "") {

							// Send Request
							$.ajax({
								type: "POST",
								url: "orders_json/save_cart/" + data.id,
								data: value,
								dataType: "json"
							});

						}

					});

					// Refresh Page
					$("body").trigger("refresh");

					// Dismiss Modal
					$("#modifyModal").modal("hide");

				}

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	// Search Customers
	$(document).on("click", "#form_customers_input input", function () {

		// Trigger AutoComplete
		$(this).autocomplete({
			appendTo: "#form_customers_input .col-sm-9",
			source: function(request, response) {
				$.ajax({
					type: "POST",
					url: "orders_json/search_users",
					data: {
						"find": request.term,
					},
					dataType: "json",
					success: function(data) {
						response($.map(data.results, function(item) {
							return {
								label : item.label,
								value : item.id,
								desc  : item.desc,
								notes : item.notes
							};
						}));
					}
				});
			},
			minLength: 0,
			select: function(event, ui) {
				$("#form_customers_input").trigger("set", [ui.item.value, ui.item.label]);
				$("#form_customer_details .col-sm-9").html(ui.item.desc);
				$("#form_customer_global_notes").val(ui.item.notes);
				return false;
			},
			open: function() {
				$("#form_customers_input .col-sm-9 ul.ui-autocomplete").addClass("dropdown-menu");
			},
			close: function() {
				$("#form_customers_input .col-sm-9 ul.ui-autocomplete").removeClass("dropdown-menu");
			}
		}).on("focus", function () {
			$("#form_customers_input input").autocomplete("search", "");
		});

	});

	// Address is Changed
	$(document).on("keyup", "#form_address,#form_city,#form_state,#form_postcode", function () {

		// Clear Toggle
		set_toggle_state($("a[data-value=same_address]"), false);

	});

	// Same Address is Clicked
	$(document).on("click", "a[data-value=same_address]", function () {

		// On?
		if ($(this).hasClass("btn-success")) {

			// Fetch Address
			$.ajax({
				type: "GET",
				url: "orders_json/get_address/" + $("#form_customers_input").triggerHandler("get"),
				dataType: "json",
				success: function(response) {

					$("#form_address").val(response.address);
					$("#form_city").val(response.city);
					$("#form_state").val(response.state);
					$("#form_postcode").val(response.postcode);

				}
			});

		}

	});

	// Detect File Upload
	$(document).on("change", "#form_attachment_input,#form_concept_input", function () {

		// Copy
		that = this;

		// Submit Form
		$("form.upload").ajaxForm({
			dataType: "json",
			success: function (data) {

				// Success?
				if (data.id > 0) {

					// Set Control
					control = $(that);

					// Add File
					$(that).parents("li.input").before(
						'<li class="list-group-item" data-id="' + data.id + '"><a href="uploads/' + data.location + '" target="_blank">' + data.name + ' (' + data.extension + '; ' + data.size + ' MB)</a><a href="#" class="pull-right" data-action="remove_upload"><span class="glyphicon glyphicon-remove"></span></a></li>'
					);

					// Reset Upload
					control.replaceWith(control = control.clone(true));

				}


			}
		}).submit();

	});

	// Remove Upload Button
	$(document).on("click", "[data-action=remove_upload]", function (event) {

		// Copy
		that = this;

		// Confirm
		if (confirm("Are you sure?")) {

			// Set ID
			id = $(this).parents("li").attr("data-id");

			// Send Request
			$.ajax({
				url: "orders_json/delete_upload/" + id,
				dataType: "json",
				success: function(response) {

					// Remove Row
					$(that).parents("li").remove();

				}
			});

		}

		// Cancel Event
		event.preventDefault();

	});

	// Detect Date Filters
	$(document).on("change", "#filter_start,#filter_end", function () {

		// Has values?
		if ($("#filter_start input").val() != "" && $("#filter_end input").val() != "") {

			// Get Values
			start = $("#filter_start").data("DateTimePicker").getDate().format("YYYY-MM-DD HH:mm:ss");
			end   = $("#filter_end").data("DateTimePicker").getDate().format("YYYY-MM-DD HH:mm:ss");

			// Redirect
			window.location.href = $(this).attr("data-url").replace("%1", start).replace("%2", end);

		}

	});

	// Request Review Button
	$(document).on("click", "#request_review", function () {

		// Copy
		that = this;

		// Confirm
		if (confirm("The customer will be emailed and asked to review the proof of concepts. Are you sure?")) {

			// Set ID
			id = $("#modifyModal #form_id").val();

			// Send Request
			$.ajax({
				url: "orders_json/request_review/" + id,
				dataType: "json",
				success: function(response) {

					// Success?
					if (response.status) {

						// Show Checkmark
						$(that).find("i").removeClass("glyphicon-envelope").addClass("glyphicon-ok").parents("a").animate({ color: "#419641" }, 500).animate({ color: "#000000" }, 500);

					} else {

						// Display Error
						alert("Failed to send email. Please try again.");

					}


				}
			});

		}

		// Cancel Event
		event.preventDefault();

	});

	// Send Approval Button
	$(document).on("click", "#send_shipping_date", function () {

		// Copy
		that = this;

		// Confirm
		if (confirm("The customer will be emailed the shipping date:\n\n" + $("#form_shipped").data("DateTimePicker").getDate().format("dddd, MMMM Do YYYY, h:mm A") + "\n\nAre you sure?")) {

			// Set ID
			id = $("#modifyModal #form_id").val();

			// Send Request
			$.ajax({
				url: "orders_json/approve_shipping/" + id + "/" + encodeURIComponent($("#form_shipped").data("DateTimePicker").getDate().format("dddd, MMMM Do YYYY, h:mm A")),
				dataType: "json",
				success: function(response) {

					// Success?
					if (response.status) {

						// Show Checkmark
						$(that).find("i").removeClass("glyphicon-envelope").addClass("glyphicon-ok").parents("a").animate({ color: "#419641" }, 500).animate({ color: "#000000" }, 500);

					} else {

						// Display Error
						alert("Failed to send email. Please try again.");

					}


				}
			});

		}

		// Cancel Event
		event.preventDefault();

	});

	/**
	 *
	 *  View Controllers
	 *
	**/

	// Refresh
	$("body").on("refresh", function (event) {

		$.ajax({
			type: "GET",
			url: $("input[name=refresh_url]").val(),
			data: {},
			dataType: "text",
			success: function(html) {

				// Replace Title
				$("title").text(html.match("<title>(.*?)</title>")[1].replace("&#187;", "»"));

				// Parse HTML DOM (Strips <head>)
				html = $.parseHTML(html);

				// Replace Pill Navigation
				$("ul.nav.nav-tabs").replaceWith($(html).find("ul.nav.nav-tabs"));

				// Replace Table
				$("table#list").replaceWith($(html).find("table#list"));

				// Replace Pagination
				$("ul.pagination").replaceWith($(html).find("ul.pagination"));

				// Count Rows in Table
				if ($("table.table tr[data-id]").length > 0) {

					// Hide Not Found
					$("#not_found").addClass("hidden");

				} else {

					// Show Not Found
					$("#not_found").removeClass("hidden");

				}

				// Highlight Search Terms
				highlightSearchWords();

			}
		});

	});

	// Delete Modal -> Show
	$("#deleteModal").on("show", function (event, values) {

		// Set ID
		$(this).find("input[name=id]").val(values.id);

		// Set Label
		$(this).find("div.modal-body").html(values.label);

		// Show
		$(this).modal();

	});

	// Modify Modal -> Show
	$("#modifyModal").on("show", function (event, values, attachments, items) {

		// Remove Errors
		$(this).find(".form-group").removeClass("has-error");

		// Clear Cart
		$("#cart_list tbody tr").remove();

		// Clear Form Controls
		$("#cart_list tfoot .form-control").val("");

		// Remove Attachments
		$("#attachments li:not(.input),#concepts li:not(.input)").remove();

		// Reset Review Button
		$("#request_review i").addClass("glyphicon-envelope").removeClass("glyphicon-ok");

		// Edit?
		if (typeof values.id !== "undefined") {

			// Enable Review Button
			$("#request_review").removeClass("disabled");

			// Set Values
			$(this).find("#form_id").val(values.id);
			$(this).find("#form_customers_input").trigger("set", [values.user_id.id, values.user_id.label]);
			$(this).find("#form_customer_details .col-sm-9").html(values.user_id.desc);
			$(this).find("#form_address").val(values.address);
			$(this).find("#form_city").val(values.city);
			$(this).find("#form_state").val(values.state);
			$(this).find("#form_postcode").val(values.postcode);
			$(this).find("#form_status [data-value=" + values.status + "] button").trigger("click");
			$(this).find("#form_customer_notes").val($("<div />").html(values.customer_notes).text());
			$(this).find("#form_internal_notes").val($("<div />").html(values.internal_notes).text());
			$(this).find("#form_customer_global_notes").val($("<div />").html(values.user_id.notes).text());
			$(this).find("#form_created").html(values.created);
			$(this).find("#form_updated").html(values.updated);
			$(this).find("#form_shipped").data("DateTimePicker").setDate(moment(values.shipped));

			// Approved?
			if ($.inArray("approved", values.options) > -1) {

				// Show Approved
				$("#form_review_status").removeClass("text-danger").addClass("text-success").html("<b>Approved for Printing</b>");

				// Disable Review Button
				$("#request_review").addClass("disabled");

			} else {

				// Show Unapproved
				$("#form_review_status").addClass("text-danger").removeClass("text-success").html("Unapproved");

			}

			// Is order and customer address same?
			var is_address_same = (values.address == values.user_id.address && values.city == values.user_id.city && values.state == values.user_id.state && values.postcode == values.user_id.postcode);

			// Set Verified Toggle
			set_toggle_state($(this).find("a[data-value=same_address]"), is_address_same);

			// Iterate Attachments
			$.each(attachments, function(index, data) {

				// Add File
				$((data.type == "attachment" ? "#attachments" : "#concepts") + " li.input").before(
					'<li class="list-group-item" data-id="' + data.id + '"><a href="uploads/' + data.location + '" target="_blank">' + data.name + " (" + data.extension + '; ' + data.size + ' MB)</a><a href="#" class="pull-right" data-action="remove_upload"><span class="glyphicon glyphicon-remove"></span></a></li>'
				);

			});

			// Iterate Cart Items
			$.each(items, function(index, value) {

				// Add Row
				$("#cartModal").trigger("add_row", [value]);

			});

		} else {

			// Disable Review Button
			$("#request_review").addClass("disabled");

			// Reset Form
			$(this).find("input,select,textarea").val("");
			$(this).find("#form_status [data-value=ordered] button").trigger("click");

			// Show Unapproved
			$("#form_review_status").addClass("text-danger").removeClass("text-success").html("Unapproved");

			// Clear Customer
			$(this).find("#form_customers_input button[data-action=clear]").trigger("click");

			// Set Verified Toggle
			set_toggle_state($(this).find("a[data-value=same_address]"), false);

			// Set Empty Dates
			$(this).find("#form_created,#form_updated").html("No Date");

			// Clear Date Picker
			$(this).find("#form_shipped").data("DateTimePicker").setDate("");

		}

		// Show
		$(this).modal();

	});

	// Modify Modal -> Get Values
	$("#modifyModal").on("get", function (event) {

		// Define
		options = [];

		// Return Values
		return {
			"id"             : $(this).find("#form_id").val(),
			"user_id"        : $("#form_customers_input").triggerHandler("get"),
			"address"        : $(this).find("#form_address").val(),
			"city"           : $(this).find("#form_city").val(),
			"state"          : $(this).find("#form_state").val(),
			"postcode"       : $(this).find("#form_postcode").val(),
			"status"         : $(this).find("#form_status").triggerHandler("get"),
			"internal_notes" : $(this).find("#form_internal_notes").val(),
			"customer_notes" : $(this).find("#form_customer_global_notes").val(),
			"attachments"    : $(".interactive-list").triggerHandler("get"),
			"shipped"        : ($(this).find("#form_shipped input").val().length < 1 ? null : $(this).find("#form_shipped").data("DateTimePicker").getDate().format("YYYY-MM-DD HH:mm:ss"))
		};

	});

	// Cart Modal -> Show
	$("#cartModal").on("show", function (event) {

		// Remove Errors
		$(this).find(".form-group").removeClass("has-error");

		// Show
		$(this).modal();

	});

	// Cart Modal -> Add Row
	$("#cartModal").on("add_row", function (event, values) {

		// Set Decoration Text
		switch (values.decoration) {
			case "screen-printing":
				decoration = "Screen Printing";
				break;
			case "embroidery":
				decoration = "Embroidery";
				break;
			case "vinyl":
				decoration = "Vinyl";
				break;
		}

		// Set Artwork Text
		switch (values.artwork) {
			case "provided":
				artwork = "Provided by Customer";
				break;
			case "in-house":
				artwork = "Created In-House";
				break;
		}

		// Create Row
		row  = '<tr data-id="' + values.id + '">';
		row += '    <td><a href="#" data-action="remove_item"><i class="glyphicon glyphicon-remove btn-sm"></i></a></td>';
		row += '    <td>' + values.quantity + '</td>';
		row += '    <td>' + values.garment + '</td>';
		row += '    <td><span style="display: inline-block; margin-bottom: -1px; background-color: ' + values.color + '; width: 12px; height: 12px; border-radius: 6px;"></span> ' + values.color + '</td>';
		row += '    <td>' + decoration + '</td>';
		row += '    <td>' + artwork + '</td>';
		row += '    <td></td>';
		row += '</tr>';

		// Attach
		$("#cart_list tbody").append(row);

		// Set Values
		$("#cart_list tbody tr:not(.input)").last().data("values", values);

	});

	// Cart Modal -> Get Values
	$("#cartModal").on("get", function (event) {

		// Define
		items = [];

		// Iterate Cart
		$("#cartModal tbody tr").each(function () {

			// Get Values
			var values = $(this).data("values");

			// Add Array Item
			items.push({
				"id"            : values.id,
				"quantity"      : values.quantity,
				"decoration"    : values.decoration,
				"color"         : values.color,
				"garment"       : values.garment,
				"artwork"       : values.artwork
			});

		});

		// Return Values
		return items;

	});

	// Attachments -> Get Values
	$(".interactive-list").on("get", function (event) {

		// Define
		items = [];

		// Iterate Cart
		$("#attachments li:not(.input),#concepts li:not(.input)").each(function () {

			// Add Array Item
			items.push($(this).attr("data-id"));

		});

		// Return Values
		return items;

	});

});