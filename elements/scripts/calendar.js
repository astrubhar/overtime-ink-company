var calendar;

$(document).ready(function () {

	/**
	 *
	 *  Interaction Events
	 *
	**/

	// Toggle List
	$(document).on("click", "#form_list a", function (event) {

		// Get Value
		value = $(this).attr("data-value");

		// Orders?
		if (value == "orders") {

			// Show Orders
			$("#schedule_view,#events_view").hide();
			$("#orders_view").show();

		}

		// Events?
		else {

			// Show Events
			$("#schedule_view,#orders_view").hide();
			$("#events_view").show();

		}

		// Cancel Event
		event.preventDefault();

	});

	// Modify Button
	$(document).on("click", "[data-action=modify]", function (event) {

		that = this;

		// Send Request
		$.ajax({
			url: "orders_json/edit/" + $(that).attr("data-event-id"),
			dataType: "json",
			success: function(response) {

				// Set Title
				html = "<h4>" + response.order.user_id.firstname + " " + response.order.user_id.lastname + " (#" + response.order.id + ")</h4>";

				// Iterate Cart Items
				$.each(response.items, function(index, value) {

					html += value.quantity + " &times; " + value.garment + " <br>";

				});

				// No cart items?
				if (response.items.length < 1) {

					html += "&mdash;";

				}

				// Set Form
				$("#form_id").val($(that).attr("data-event-id"));
				$("#form_details").html(html);
				$("#form_start").data("DateTimePicker").setDate(response.order.start);
				$("#form_end").data("DateTimePicker").setDate(response.order.end);
				$("#form_status").val(response.order.status);

				// Hide Orders
				$("#orders_view,#events_view").hide();

				// Show Schedule
				$("#schedule_view").show();

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	// Save Button
	$(document).on("click", "[data-action=save]", function (event) {

		// Get Values
		values = {
			"id"     : $("#form_id").val(),
			"start"  : ($("#form_start input").val() == "" ? null : $("#form_start").data("DateTimePicker").getDate().format("YYYY-MM-DD HH:mm:ss")),
			"end"    : ($("#form_end input").val() == "" ? null : $("#form_end").data("DateTimePicker").getDate().format("YYYY-MM-DD HH:mm:ss")),
			"status" : $("#form_status").val(),
		};

		// Send Request
		$.ajax({
			type: "POST",
			url: "orders_json/save_schedule/" + values.id,
			data: values,
			dataType: "json",
			success: function(data) {

				// Errors?
				if (data.errors.length > 0) {

					// Clear Errors
					$("#schedule_view").find(".form-group").removeClass("has-error");

					// Iterate Errors
					$.each(data.errors, function(index, value) {

						// Mark Error
						$("#schedule_view").find("#form_" + value).parents(".form-group").addClass("has-error");

					});

				}

				// Success?
				else {

					// Refresh Calendar
					$("body").trigger("refresh");

					// Hide Schedule
					$("#schedule_view").hide();

					// Show orders?
					if ($("#form_list").triggerHandler("get") == "orders") {

						// Show Orders
						$("#orders_view").show();

					} else {

						// Show Events
						$("#events_view").show();

					}

				}

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	// Cancel Button
	$(document).on("click", "[data-action=cancel]", function (event) {

		// Hide Schedule
		$("#schedule_view").hide();

		// Show orders?
		if ($("#form_list").triggerHandler("get") == "orders") {

			// Show Orders
			$("#orders_view").show();

		} else {

			// Show Events
			$("#events_view").show();

		}

		// Cancel Event
		event.preventDefault();

	});

	// Calendar Prev/Today/Next Nagivation
	$('.btn-group button[data-calendar-nav]').on("click", function () {

		// Set Calendar Navigation
		calendar.navigate($(this).attr('data-calendar-nav'));

	});

	// Calendar Year/Month/Week/Day View
	$('.btn-group button[data-calendar-view]').on("click", function () {

		// Set Calendar View
		calendar.view($(this).attr('data-calendar-view'));

	});

	/**
	 *
	 *  View Controllers
	 *
	**/

	// Refresh
	$("body").on("refresh", function (event) {

		// Create Calendar
		calendar = $('#calendar').calendar({
			events_source: 'calendar_json/events/' + $("#status_filter").val(),
			view: 'month',
			tmpl_path: 'elements/templates/calendar/',
			tmpl_cache: false,
			onAfterEventsLoad: function(events) {
				if(!events) {
					return;
				}
				var list = $('#events');
				list.html('');

				if (events.length < 1) {
					$('<h4>No Orders</h4>').appendTo(list);
				}

				$.each(events, function(key, val) {
					$('<a href="' + val.url + '" class="list-group-item" data-event-id="' + val.id + '" data-action="modify"><span class="pull-left event ' + val["class"] + '" style="margin: 4px 4px 0px 0px;"></span> ' + val.title + '</a>')
						.appendTo(list);
				});
			},
			onAfterViewLoad: function(view) {
				$('h3#title').text(this.getTitle());
				$('.btn-group button').removeClass('active');
				$('button[data-calendar-view="' + view + '"]').addClass('active');
			},
			classes: {
				months: {
					general: 'label'
				}
			}
		});

		// Highlight Search Terms
		highlightSearchWords();

	});

	// Load Calendar Immediately
	$("body").trigger("refresh");

});