<?php

class Email {
	
	public function send($from, $to, $subject, $text_body, $html_body, $attachments = array()) {
	
		// MIME Boundary
		$mime_boundary = md5(time());
	
		// Compose Email
		$headers = "From: \"{$from["name"]}\" <{$from["email"]}>\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: multipart/mixed; boundary=\"MIXED-{$mime_boundary}\"\r\n\r\n";
	
		// Multipart Header
		$message = "--MIXED-{$mime_boundary}\r\n";
		$message .= "Content-Type: multipart/alternative; boundary=\"ALT-{$mime_boundary}\"\r\n\r\n";
	
		// Text Version
		$message .= "--ALT-{$mime_boundary}\r\n";
		$message .= "Content-Type: text/plain; charset=UTF-8\r\n";
		$message .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message .= "{$text_body}\r\n\r\n";
	
		// HTML Version
		$message .= "--ALT-{$mime_boundary}\r\n";
		$message .= "Content-Type: text/html; charset=UTF-8\r\n";
		$message .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message .= str_replace("\t", "", $html_body) . "\r\n";
		$message .= "--ALT-{$mime_boundary}--\r\n\r\n";
	
		// File Attachments
		foreach ($attachments as $attachment) {
	
			$message .= "--MIXED-{$mime_boundary}\r\n";
			$message .= "Content-Type: {$attachment["type"]}; name=\"{$attachment["name"]}\"\r\n";
			$message .= "Content-Transfer-Encoding: base64\r\n";
			$message .= "Content-Disposition: attachment\r\n\r\n";
			$message .= chunk_split(base64_encode(file_get_contents($attachment["path"]))) . "\r\n\r\n";
	
		}
	
		// Close Email
		$message .= "--MIXED-{$mime_boundary}--\r\n";
	
		// Send Email
		foreach ($to as $recipient) {
	
			mail("\"{$recipient["name"]}\" <{$recipient["email"]}>", $subject, $message, $headers);
	
		}
	
	}
	
}