<input type="hidden" name="highlight_terms" value="<?=$highlight?>">
<input type="hidden" name="refresh_url" value="users/index/<?=$country?>/<?=$state?>/<?=$role?>/<?=$page?>/<?=urlencode($find)?>">

<div class="container">
	<div class="row">

		<div class="col-md-3" style="padding-bottom: 5px;">

			<a data-action="create" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i> Create <?=($_SESSION["role"] == "administrator" ? "User" : "Customer")?></a>

			<select class="form-control" data-action="filter" data-url="users/index/%?/<?=$state?>/<?=$role?>/1/<?=urlencode($find)?>" style="margin-top: 20px;">
				<?=$this->replace()?>
				<option value="any">All Countries</option>
				<option value="US">United States</option>
				<option value="CA">Canada</option>
				<?=$this->replace("\"{$country}\"", "\"{$country}\" selected")?>
			</select>

			<select class="form-control" data-action="filter" data-url="users/index/<?=$country?>/%?/<?=$role?>/1/<?=urlencode($find)?>" style="margin-top: 20px;">
				<?=$this->replace()?>
				<option value="any">All States/Provinces</option>
				<optgroup label="United States">
					<option value="US-AL">Alabama</option>
					<option value="US-AK">Alaska</option>
					<option value="US-AZ">Arizona</option>
					<option value="US-AR">Arkansas</option>
					<option value="US-CA">California</option>
					<option value="US-CO">Colorado</option>
					<option value="US-CT">Connecticut</option>
					<option value="US-DE">Delaware</option>
					<option value="US-FL">Florida</option>
					<option value="US-GA">Georgia</option>
					<option value="US-HI">Hawaii</option>
					<option value="US-ID">Idaho</option>
					<option value="US-IL">Illinois</option>
					<option value="US-IN">Indiana</option>
					<option value="US-IA">Iowa</option>
					<option value="US-KS">Kansas</option>
					<option value="US-KY">Kentucky</option>
					<option value="US-LA">Louisiana</option>
					<option value="US-ME">Maine</option>
					<option value="US-MD">Maryland</option>
					<option value="US-MA">Massachusetts</option>
					<option value="US-MI">Michigan</option>
					<option value="US-MN">Minnesota</option>
					<option value="US-MS">Mississippi</option>
					<option value="US-MO">Missouri</option>
					<option value="US-MT">Montana</option>
					<option value="US-NE">Nebraska</option>
					<option value="US-NV">Nevada</option>
					<option value="US-NH">New Hampshire</option>
					<option value="US-NJ">New Jersey</option>
					<option value="US-NM">New Mexico</option>
					<option value="US-NY">New York</option>
					<option value="US-NC">North Carolina</option>
					<option value="US-ND">North Dakota</option>
					<option value="US-OH">Ohio</option>
					<option value="US-OK">Oklahoma</option>
					<option value="US-OR">Oregon</option>
					<option value="US-PA">Pennsylvania</option>
					<option value="US-RI">Rhode Island</option>
					<option value="US-SC">South Carolina</option>
					<option value="US-SD">South Dakota</option>
					<option value="US-TN">Tennessee</option>
					<option value="US-TX">Texas</option>
					<option value="US-UT">Utah</option>
					<option value="US-VT">Vermont</option>
					<option value="US-VA">Virginia</option>
					<option value="US-WA">Washington</option>
					<option value="US-WV">West Virginia</option>
					<option value="US-WI">Wisconsin</option>
					<option value="US-WY">Wyoming</option>
				</optgroup>
				<optgroup label="Canada">
					<option value="CA-AB">Alberta</option>
					<option value="CA-BC">British Columbia</option>
					<option value="CA-MB">Manitoba</option>
					<option value="CA-NB">New Brunswick</option>
					<option value="CA-NL">Newfoundland and Labrador</option>
					<option value="CA-NS">Nova Scotia</option>
					<option value="CA-ON">Ontario</option>
					<option value="CA-PE">Prince Edward Island</option>
					<option value="CA-SK">Saskatchewan</option>
				</optgroup>
				<optgroup label="US Territory">
					<option value="US-AS">American Samoa</option>
					<option value="US-DC">District of Columbia</option>
					<option value="US-FM">Federated States of Micronesia</option>
					<option value="US-GU">Guam</option>
					<option value="US-MH">Marshall Islands</option>
					<option value="US-MP">Northern Mariana Islands</option>
					<option value="US-PW">Palau</option>
					<option value="US-PR">Puerto Rico</option>
					<option value="US-VI">Virgin Islands</option>
				</optgroup>
				<optgroup label="US Military State">
					<option value="US-AEA">Armed Forces Africa</option>
					<option value="US-AA">Armed Forces Americas</option>
					<option value="US-AEC">Armed Forces Canada</option>
					<option value="US-AEE">Armed Forces Europe</option>
					<option value="US-AEM">Armed Forces Middle East</option>
					<option value="US-AP">Armed Forces Pacific</option>
				</optgroup>
				<optgroup label="CA Territory">
					<option value="CA-NT">Northwest Territories</option>
					<option value="CA-NU">Nunavut</option>
					<option value="CA-YT">Yukon</option>
				</optgroup>
				<?=$this->replace("\"{$state}\"", "\"{$state}\" selected")?>
			</select>

			<? if ($_SESSION["role"] == "administrator"): ?>
			<select class="form-control" data-action="filter" data-url="users/index/<?=$country?>/<?=$state?>/%?/1/<?=urlencode($find)?>" style="margin-top: 20px;">
				<?=$this->replace()?>
				<option value="any">All Roles</option>
				<option value="customer">Customers</option>
				<option value="staff">Staff</option>
				<option value="administrator">Administrators</option>
				<?=$this->replace("\"{$role}\"", "\"{$role}\" selected")?>
			</select>
			<? endif ?>

		</div>

		<div class="col-md-9">

			<table class="table table-striped" style="table-layout: fixed; white-space: nowrap;">
				<tr>
					<th style="width: 60px;"></th>
					<th style="width: 140px;">Name</th>
					<th class="hidden-sm hidden-xs">Email</th>
					<th style="width: 180px;" class="hidden-xs">City/State/Country</th>
					<th style="width: 160px;" class="hidden-md hidden-sm hidden-xs">Last Login</th>
					<th style="width: 60px;"></th>
				</tr>
				<? foreach ($users as $user): ?>
				<tr data-id="<?=$user->id?>">
					<td><a href="#" data-action="delete" data-id="<?=$user->id?>" data-label="<?=$user->firstname?> <?=$user->lastname?>" class="btn btn-danger btn-xs">Delete</a></td>
					<td><?=$user->firstname?> <?=$user->lastname?></td>
					<td class="hidden-sm hidden-xs"><?=$user->email?></td>
					<? list($country, $state) = explode("-", $user->state) ?>
					<td class="hidden-xs"><?=trim("{$user->city}, {$state}, {$country}", ", ")?></td>
					<td class="hidden-md hidden-sm hidden-xs"><?=$timeago->inWords($user->last_login, "now")?> </td>
					<td><a href="#" data-action="modify" data-id="<?=$user->id?>" class="btn btn-primary btn-xs">Modify</a></td>
				</tr>
				<? endforeach ?>
			</table>

			<div id="not_found" class="alert alert-warning text-center <? if (!empty($users)): ?>hidden<? endif ?>">
				<span class="alert-link">No <?=($_SESSION["role"] == "administrator" ? "Users" : "Customers")?> Match Filters</span>
			</div>

			<? if ($pages > 1): ?>
			<ul class="pagination">

				<? if ($start > 1): ?>
				<li><a href="users/index/<?=$country?>/<?=$state?>/<?=$role?>/1/<?=urlencode($find)?>">1</a></li>
				<? endif ?>

				<? for ($i = $start; $i <= $end; $i++): ?>
				<li<? if ($i == $page): ?> class="active"<? endif ?>><a href="users/index/<?=$country?>/<?=$state?>/<?=$role?>/<?=$i?>/<?=urlencode($find)?>"><?=$i?></a></li>
				<? endfor ?>

				<? if ($end < $pages): ?>
				<li><a href="users/index/<?=$country?>/<?=$state?>/<?=$role?>/<?=$pages?>/<?=urlencode($find)?>"><?=$pages?></a></li>
				<? endif ?>

			</ul>
			<? endif ?>

		</div>
	</div>
</div>

<div class="modal fade" id="modifyModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-body" style="padding-top: 35px;">

				<form class="form-horizontal" role="form">

					<div class="row">

						<div class="col-sm-7">

							<div class="form-group">
								<label for="form_id" class="col-sm-3 control-label" id="form_id_label">ID</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="form_id" placeholder="Not Saved" disabled>
								</div>
							</div>

							<div class="form-group">
								<label for="form_company" class="col-sm-3 control-label" id="form_company_label">Company</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="form_company">
								</div>
							</div>

							<div class="form-group">
								<label for="form_firstname" class="col-sm-3 control-label" id="form_name_label">Name</label>
								<div class="col-sm-9">
									<div class="input-group">
										<input type="text" class="form-control" id="form_firstname" placeholder="First" style="width: 50%;">
										<input type="text" class="form-control" id="form_lastname" placeholder="Last" style="width: 50%;">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="form_address" class="col-sm-3 control-label" id="form_address_label">Address</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="form_address">
								</div>
							</div>

							<div class="form-group">
								<label for="form_address" class="col-sm-3 control-label" id="form_name_label"></label>
								<div class="col-sm-9">
									<div class="input-group">
										<input type="text" class="form-control" id="form_city" placeholder="City" style="width: 30%;">
										<select class="form-control" id="form_state" style="width: 40%;">
											<option value="">State/Province</option>
											<optgroup label="United States">
												<option value="US-AL">Alabama</option>
												<option value="US-AK">Alaska</option>
												<option value="US-AZ">Arizona</option>
												<option value="US-AR">Arkansas</option>
												<option value="US-CA">California</option>
												<option value="US-CO">Colorado</option>
												<option value="US-CT">Connecticut</option>
												<option value="US-DE">Delaware</option>
												<option value="US-FL">Florida</option>
												<option value="US-GA">Georgia</option>
												<option value="US-HI">Hawaii</option>
												<option value="US-ID">Idaho</option>
												<option value="US-IL">Illinois</option>
												<option value="US-IN">Indiana</option>
												<option value="US-IA">Iowa</option>
												<option value="US-KS">Kansas</option>
												<option value="US-KY">Kentucky</option>
												<option value="US-LA">Louisiana</option>
												<option value="US-ME">Maine</option>
												<option value="US-MD">Maryland</option>
												<option value="US-MA">Massachusetts</option>
												<option value="US-MI">Michigan</option>
												<option value="US-MN">Minnesota</option>
												<option value="US-MS">Mississippi</option>
												<option value="US-MO">Missouri</option>
												<option value="US-MT">Montana</option>
												<option value="US-NE">Nebraska</option>
												<option value="US-NV">Nevada</option>
												<option value="US-NH">New Hampshire</option>
												<option value="US-NJ">New Jersey</option>
												<option value="US-NM">New Mexico</option>
												<option value="US-NY">New York</option>
												<option value="US-NC">North Carolina</option>
												<option value="US-ND">North Dakota</option>
												<option value="US-OH">Ohio</option>
												<option value="US-OK">Oklahoma</option>
												<option value="US-OR">Oregon</option>
												<option value="US-PA">Pennsylvania</option>
												<option value="US-RI">Rhode Island</option>
												<option value="US-SC">South Carolina</option>
												<option value="US-SD">South Dakota</option>
												<option value="US-TN">Tennessee</option>
												<option value="US-TX">Texas</option>
												<option value="US-UT">Utah</option>
												<option value="US-VT">Vermont</option>
												<option value="US-VA">Virginia</option>
												<option value="US-WA">Washington</option>
												<option value="US-WV">West Virginia</option>
												<option value="US-WI">Wisconsin</option>
												<option value="US-WY">Wyoming</option>
											</optgroup>
											<optgroup label="Canada">
												<option value="CA-AB">Alberta</option>
												<option value="CA-BC">British Columbia</option>
												<option value="CA-MB">Manitoba</option>
												<option value="CA-NB">New Brunswick</option>
												<option value="CA-NL">Newfoundland and Labrador</option>
												<option value="CA-NS">Nova Scotia</option>
												<option value="CA-ON">Ontario</option>
												<option value="CA-PE">Prince Edward Island</option>
												<option value="CA-SK">Saskatchewan</option>
											</optgroup>
											<optgroup label="US Territory">
												<option value="US-AS">American Samoa</option>
												<option value="US-DC">District of Columbia</option>
												<option value="US-FM">Federated States of Micronesia</option>
												<option value="US-GU">Guam</option>
												<option value="US-MH">Marshall Islands</option>
												<option value="US-MP">Northern Mariana Islands</option>
												<option value="US-PW">Palau</option>
												<option value="US-PR">Puerto Rico</option>
												<option value="US-VI">Virgin Islands</option>
											</optgroup>
											<optgroup label="US Military State">
												<option value="US-AEA">Armed Forces Africa</option>
												<option value="US-AA">Armed Forces Americas</option>
												<option value="US-AEC">Armed Forces Canada</option>
												<option value="US-AEE">Armed Forces Europe</option>
												<option value="US-AEM">Armed Forces Middle East</option>
												<option value="US-AP">Armed Forces Pacific</option>
											</optgroup>
											<optgroup label="CA Territory">
												<option value="CA-NT">Northwest Territories</option>
												<option value="CA-NU">Nunavut</option>
												<option value="CA-YT">Yukon</option>
											</optgroup>
										</select>
										<input type="text" class="form-control" id="form_postcode" placeholder="Post Code" style="width: 30%;">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="form_email" class="col-sm-3 control-label" id="form_email_label">Email</label>
								<div class="col-sm-7">
									<input type="email" class="form-control" id="form_email" data-action="update_email">
								</div>
								<div class="col-sm-1">
									<a class="btn btn-primary hidden-xs" id="form_send_email"><i class="glyphicon glyphicon-envelope"></i></a>
								</div>
							</div>

							<div class="form-group">
								<label for="form_password" class="col-sm-3 control-label" id="form_password_label">Password</label>
								<div class="col-sm-7">
									<input type="text" class="form-control" id="form_password" placeholder="Set Password">
								</div>
								<div class="col-sm-1">
									<a class="btn btn-danger hidden-xs" data-action="erase_password" id="form_erase_password"><i class="glyphicon glyphicon-ban-circle"></i></a>
								</div>
							</div>

						</div>
						<div class="col-sm-5" style="padding-top: 49px;">

							<div class="form-group">
								<label for="form_phone" class="col-sm-4 control-label" id="form_phone_label">Phone</label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" class="form-control" id="form_phone" style="width: 75%;">
										<input type="text" class="form-control" id="form_phone_ext" placeholder="Ext" style="width: 25%;">
									</div>
								</div>
							</div>

							<? if ($_SESSION["role"] == "administrator"): ?>
							<div class="form-group">
								<label for="form_role" class="col-sm-4 control-label" id="form_role_label">Role</label>
								<div class="col-sm-6">
									<select class="form-control" id="form_role">
										<option value="customer">Customer</option>
										<option value="staff">Staff</option>
										<option value="administrator">Administrator</option>
									</select>
								</div>
							</div>
							<? else: ?>
							<div class="form-group">
								<label for="form_role" class="col-sm-4 control-label" id="form_role_label">Role</label>
								<div class="col-sm-6">
									<p class="form-control-static" style="height: 34px;">Customer</p>
									<input type="hidden" name="form_role" value="customer">
								</div>
							</div>
							<? endif ?>

							<div class="form-group">
								<label class="col-sm-4 control-label">Options</label>
								<div class="col-sm-6">
									<a data-action="toggle" class="btn btn-default" data-value="verified"><i class="glyphicon glyphicon-unchecked"></i> Email Verified</a>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-4 control-label">Created</label>
								<div class="col-sm-6">
									<p class="form-control-static" id="form_created" style="height: 34px;"></p>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-4 control-label">Updated</label>
								<div class="col-sm-6">
									<p class="form-control-static" id="form_updated" style="height: 34px;"></p>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-4 control-label">Last Login</label>
								<div class="col-sm-6">
									<p class="form-control-static" id="form_last_login" style="height: 34px;"></p>
								</div>
							</div>

						</div>

					</div>

					<br><hr style="margin-top: 0px;">


					<div class="form-group" style="margin: 0px;">
						<label class="control-label">Notes about Customer</label>
						<textarea class="form-control" id="form_notes" style="margin-top: 20px; height: 120px;"></textarea>
					</div>

				</form>

			</div>

			<div class="modal-footer" id="form_actions">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal"></span>Cancel</button>
				<button type="button" class="btn btn-success" data-action="save"></span>Save</button>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<input type="hidden" name="id" value="">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete <?=($_SESSION["role"] == "administrator" ? "user" : "customer")?>?</h4>
			</div>

			<div class="modal-body">

				Label

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger" data-action="confirm_delete">Delete</button>
			</div>

		</div>
	</div>
</div>

<script defer src="elements/scripts/users.js"></script>