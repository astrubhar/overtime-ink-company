<input type="hidden" name="highlight_terms" value="<?=$highlight?>">
<input type="hidden" name="refresh_url" value="orders/index/<?=$status?>/<?=$filter_start?>/<?=$filter_end?>/<?=$page?>/<?=urlencode($find)?>">

<div class="container">
	<div class="row">

		<div class="col-md-3" style="padding-bottom: 5px;">

			<a data-action="create" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i> Create Order</a>

			<div class="form-group" style="margin-top: 20px;">
			    <div class="input-group date" id="filter_start" data-url="orders/index/<?=$status?>/%1/%2/1/<?=urlencode($find)?>">
			        <input type="text" class="form-control" placeholder="Start Date"<? if ($filter_start != "+"): ?> value="<?=date("m/d/Y g:i A", strtotime(str_replace("_", "-", $filter_start)))?>"<? endif ?>>
			        <span class="input-group-addon">
			        	<span class="glyphicon-calendar glyphicon"></span>
			        </span>
			    </div>
			</div>

			<div class="form-group" style="margin-top: 20px;">
			    <div class="input-group date" id="filter_end" data-url="orders/index/<?=$status?>/%1/%2/1/<?=urlencode($find)?>">
			        <input type="text" class="form-control" placeholder="End Date"<? if ($filter_end != "+"): ?> value="<?=date("m/d/Y g:i A", strtotime(str_replace("_", "-", $filter_end)))?>"<? endif ?>>
			        <span class="input-group-addon">
			        	<span class="glyphicon-calendar glyphicon"></span>
			        </span>
			    </div>
			</div>

			<? if ($filter_start != "+" and $filter_end != "+"): ?>
			<a href="orders/index/<?=$status?>/+/+/1/<?=urlencode($find)?>" class="btn btn-success btn-block">Clear Date Filter</a>
			<? endif ?>

		</div>

		<div class="col-md-9">

			<ul class="nav nav-tabs small">
				<li class="<?=($status == "all" ? "active" : "")?>"><a href="orders/index/all/<?=$filter_start?>/<?=$filter_end?>/1/<?=urlencode($find)?>">All (<?=number_format($counts["all"], 0)?>)</a></li>
				<li class="<?=($status == "ordered" ? "active" : "")?>"><a href="orders/index/ordered/<?=$filter_start?>/<?=$filter_end?>/1/<?=urlencode($find)?>">Ordered (<?=number_format($counts["ordered"], 0)?>)</a></li>
				<li class="<?=($status == "supplied" ? "active" : "")?>"><a href="orders/index/supplied/<?=$filter_start?>/<?=$filter_end?>/1/<?=urlencode($find)?>">Supplied (<?=number_format($counts["supplied"], 0)?>)</a></li>
				<li class="<?=($status == "scheduled" ? "active" : "")?>"><a href="orders/index/scheduled/<?=$filter_start?>/<?=$filter_end?>/1/<?=urlencode($find)?>">Scheduled (<?=number_format($counts["scheduled"], 0)?>)</a></li>
				<li class="<?=($status == "completed" ? "active" : "")?>"><a href="orders/index/completed/<?=$filter_start?>/<?=$filter_end?>/1/<?=urlencode($find)?>">Completed (<?=number_format($counts["completed"], 0)?>)</a></li>
				<li class="<?=($status == "shipped" ? "active" : "")?>"><a href="orders/index/shipped/<?=$filter_start?>/<?=$filter_end?>/1/<?=urlencode($find)?>">Shipped (<?=number_format($counts["shipped"], 0)?>)</a></li>
			</ul>

			<table id="list" class="table table-striped" style="margin-top: -1px; table-layout: fixed; white-space: nowrap;">
				<tr>
					<th style="width: 60px;"></th>
					<th style="width: 160px;">Name</th>
					<th class="hidden-xs">Address</th>
					<th class="hidden-xs" style="width: 160px;">City/State/Country</th>
					<th class="hidden-md hidden-sm hidden-xs" style="width: 140px;">Ordered</th>
					<th style="width: 100px;">Status</th>
					<th style="width: 65px;"></th>
				</tr>
				<? foreach ($orders as $order): ?>
				<?

				// Status
				switch ($order->status) {

					case "pending":
					case "ordered":
						$row_class = "danger";
						break;

					case "supplied":
						$row_class = "warning";
						break;

					case "scheduled":
						$row_class = "success";
						break;

					case "completed":
						$row_class = "purple";
						break;

					case "shipped":
						$row_class = "info";
						break;

				}

				?>
				<tr data-id="<?=$order->id?>" class="<?=$row_class?>">
					<td><a href="#" data-action="delete" data-id="<?=$order->id?>" data-label="Order #<?=$order->id?> (by <?=$order->user_id->firstname?> <?=$order->user_id->lastname?> at <?=$order->created?>)" class="btn btn-danger btn-xs">Delete</a></td>
					<td><?=$order->user_id->firstname?> <?=$order->user_id->lastname?></td>
					<td class="hidden-xs"><?=$order->address?></td>
					<? list($country, $state) = explode("-", $order->state) ?>
					<td class="hidden-xs"><?=trim("{$order->city}, {$state}, {$country}", ", ")?></td>
					<td class="hidden-md hidden-sm hidden-xs"><?=$timeago->inWords($order->created, "now")?></td>
					<td><?=ucwords($order->status)?>
					<td><a href="#" data-action="modify" data-id="<?=$order->id?>" class="btn btn-primary btn-xs">Modify</a></td>
				</tr>
				<? endforeach ?>
			</table>

			<div id="not_found" class="alert alert-warning text-center <? if (!empty($orders)): ?>hidden<? endif ?>">
				<span class="alert-link">No Orders Match Filters</span>
			</div>

			<? if ($pages > 1): ?>
			<ul class="pagination">

				<? if ($start > 1): ?>
				<li><a href="orders/index/<?=$status?>/<?=$filter_start?>/<?=$filter_end?>/1/<?=urlencode($find)?>">1</a></li>
				<? endif ?>

				<? for ($i = $start; $i <= $end; $i++): ?>
				<li<? if ($i == $page): ?> class="active"<? endif ?>><a href="orders/index/<?=$status?>/<?=$filter_start?>/<?=$filter_end?>/<?=$i?>/<?=urlencode($find)?>"><?=$i?></a></li>
				<? endfor ?>

				<? if ($end < $pages): ?>
				<li><a href="orders/index/<?=$status?>/<?=$filter_start?>/<?=$filter_end?>/<?=$pages?>/<?=urlencode($find)?>"><?=$pages?></a></li>
				<? endif ?>

			</ul>
			<? endif ?>

		</div>
	</div>
</div>

<div class="modal fade" id="modifyModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-body" style="padding-top: 20px;">

				<form class="form-horizontal upload" role="form" enctype="multipart/form-data" method="post" action="orders_json/upload_file">

					<span class="small">
						<div class="stepwizard" id="form_status">
						    <div class="stepwizard-row">
						        <div style="width: 20%" data-value="ordered" class="stepwizard-step"><button type="button" class="btn btn-success btn-circle">1</button><p>Ordered</p></div>
						        <div style="width: 20%" data-value="supplied" class="stepwizard-step"><button type="button" class="btn btn-default btn-circle">2</button><p>Garments Received</p></div>
						        <div style="width: 20%" data-value="scheduled" class="stepwizard-step"><button type="button" class="btn btn-default btn-circle">3</button><p>Production Scheduled</p></div>
						        <div style="width: 20%" data-value="completed" class="stepwizard-step"><button type="button" class="btn btn-default btn-circle">4</button><p>Production Completed</p></div>
						        <div style="width: 20%" data-value="shipped" class="stepwizard-step"><button type="button" class="btn btn-default btn-circle">5</button><p>Shipped to Customer</p></div>
						    </div>
						</div>
					</span>

					<hr style="margin-top: 0px;"><br>

					<div class="row">

						<div class="col-sm-7">

							<div class="form-group">
								<label for="form_id" class="col-sm-3 control-label" id="form_id_label">Order ID</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="form_id" placeholder="Not Saved" disabled>
								</div>
							</div>

							<div class="form-group find" id="form_customers_input" data-id="">
								<label for="form_customers_input" class="col-sm-3 control-label">Customer</label>
								<div class="col-sm-9" style="position: relative;">
									<div class="input-group">
										<input type="text" class="form-control ui-autocomplete-input" placeholder="Search (Name, Email, Phone, Address)" autocomplete="off">
										<span class="input-group-btn">
											<button data-action="clear" class="btn btn-default" disabled="disabled"><i class="glyphicon glyphicon-remove"></i></button>
										</span>
									</div>
								<ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all" id="ui-id-1" tabindex="0" style="display: none; width: 300px; position: relative; top: 0px; left: 0.5px;"><li class="ui-menu-item" role="presentation"><a id="ui-id-3" class="ui-corner-all" tabindex="-1">Andrew Strubhar, 1 Main, 123, Wichita, US-KS</a></li><li class="ui-menu-item" role="presentation"><a id="ui-id-4" class="ui-corner-all" tabindex="-1">Organization, 123 Huge Street, Findlay, US-OH</a></li></ul></div>
							</div>

							<div class="form-group" id="form_customer_details">
								<label class="col-sm-3 control-label"></label>
								<div class="col-sm-9"></div>
							</div>

							<div class="form-group">
								<label for="form_address" class="col-sm-3 control-label" id="form_address_label">Address</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="form_address">
								</div>
							</div>

							<div class="form-group">
								<label for="form_address" class="col-sm-3 control-label" id="form_name_label"></label>
								<div class="col-sm-9">
									<div class="input-group">
										<input type="text" class="form-control" id="form_city" placeholder="City" style="width: 30%;">
										<select class="form-control" id="form_state" style="width: 40%;">
											<option value="">State/Province</option>
											<optgroup label="United States">
												<option value="US-AL">Alabama</option>
												<option value="US-AK">Alaska</option>
												<option value="US-AZ">Arizona</option>
												<option value="US-AR">Arkansas</option>
												<option value="US-CA">California</option>
												<option value="US-CO">Colorado</option>
												<option value="US-CT">Connecticut</option>
												<option value="US-DE">Delaware</option>
												<option value="US-FL">Florida</option>
												<option value="US-GA">Georgia</option>
												<option value="US-HI">Hawaii</option>
												<option value="US-ID">Idaho</option>
												<option value="US-IL">Illinois</option>
												<option value="US-IN">Indiana</option>
												<option value="US-IA">Iowa</option>
												<option value="US-KS">Kansas</option>
												<option value="US-KY">Kentucky</option>
												<option value="US-LA">Louisiana</option>
												<option value="US-ME">Maine</option>
												<option value="US-MD">Maryland</option>
												<option value="US-MA">Massachusetts</option>
												<option value="US-MI">Michigan</option>
												<option value="US-MN">Minnesota</option>
												<option value="US-MS">Mississippi</option>
												<option value="US-MO">Missouri</option>
												<option value="US-MT">Montana</option>
												<option value="US-NE">Nebraska</option>
												<option value="US-NV">Nevada</option>
												<option value="US-NH">New Hampshire</option>
												<option value="US-NJ">New Jersey</option>
												<option value="US-NM">New Mexico</option>
												<option value="US-NY">New York</option>
												<option value="US-NC">North Carolina</option>
												<option value="US-ND">North Dakota</option>
												<option value="US-OH">Ohio</option>
												<option value="US-OK">Oklahoma</option>
												<option value="US-OR">Oregon</option>
												<option value="US-PA">Pennsylvania</option>
												<option value="US-RI">Rhode Island</option>
												<option value="US-SC">South Carolina</option>
												<option value="US-SD">South Dakota</option>
												<option value="US-TN">Tennessee</option>
												<option value="US-TX">Texas</option>
												<option value="US-UT">Utah</option>
												<option value="US-VT">Vermont</option>
												<option value="US-VA">Virginia</option>
												<option value="US-WA">Washington</option>
												<option value="US-WV">West Virginia</option>
												<option value="US-WI">Wisconsin</option>
												<option value="US-WY">Wyoming</option>
											</optgroup>
											<optgroup label="Canada">
												<option value="CA-AB">Alberta</option>
												<option value="CA-BC">British Columbia</option>
												<option value="CA-MB">Manitoba</option>
												<option value="CA-NB">New Brunswick</option>
												<option value="CA-NL">Newfoundland and Labrador</option>
												<option value="CA-NS">Nova Scotia</option>
												<option value="CA-ON">Ontario</option>
												<option value="CA-PE">Prince Edward Island</option>
												<option value="CA-SK">Saskatchewan</option>
											</optgroup>
											<optgroup label="US Territory">
												<option value="US-AS">American Samoa</option>
												<option value="US-DC">District of Columbia</option>
												<option value="US-FM">Federated States of Micronesia</option>
												<option value="US-GU">Guam</option>
												<option value="US-MH">Marshall Islands</option>
												<option value="US-MP">Northern Mariana Islands</option>
												<option value="US-PW">Palau</option>
												<option value="US-PR">Puerto Rico</option>
												<option value="US-VI">Virgin Islands</option>
											</optgroup>
											<optgroup label="US Military State">
												<option value="US-AEA">Armed Forces Africa</option>
												<option value="US-AA">Armed Forces Americas</option>
												<option value="US-AEC">Armed Forces Canada</option>
												<option value="US-AEE">Armed Forces Europe</option>
												<option value="US-AEM">Armed Forces Middle East</option>
												<option value="US-AP">Armed Forces Pacific</option>
											</optgroup>
											<optgroup label="CA Territory">
												<option value="CA-NT">Northwest Territories</option>
												<option value="CA-NU">Nunavut</option>
												<option value="CA-YT">Yukon</option>
											</optgroup>
										</select>
										<input type="text" class="form-control" id="form_postcode" placeholder="Post Code" style="width: 30%;">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label"></label>
								<div class="col-sm-9">
									<a data-action="toggle" class="btn btn-default" data-value="same_address"><i class="glyphicon glyphicon-unchecked"></i> Use Address on File</a>
								</div>
							</div>

							<div class="form-group">
								<label for="form_address" class="col-sm-3 control-label" id="form_address_label">Shipping Date</label>
								<div class="col-sm-9">
								    <a class="btn btn-default pull-right" style="width: 135px;" id="send_shipping_date"><i class="glyphicon glyphicon-envelope"></i> Approve Date</a>
								    <div class="input-group date" id="form_shipped" style="width: 210px;">
								        <input type="text" class="form-control" placeholder="No Date">
								        <span class="input-group-addon">
								        	<span class="glyphicon-calendar glyphicon"></span>
								        </span>
								    </div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Ordered</label>
								<div class="col-sm-6">
									<p class="form-control-static" id="form_created"></p>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Last Updated</label>
								<div class="col-sm-6">
									<p class="form-control-static" id="form_updated"></p>
								</div>
							</div>

						</div>
						<div class="col-sm-5">

							<div class="form-group">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<a class="btn btn-success btn-block" data-action="view_cart"><i class="glyphicon glyphicon-th-list"></i> Garments, Colors, and Quantities</a>
								</div>
								<div class="col-sm-1"></div>
							</div>

							<div class="form-group">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label for="form_attachment_input">Order Attachments</label>
									<ul class="list-group interactive-list" id="attachments" style="position: relative; margin-bottom: 0px;">
										<li class="list-group-item input"><input type="file" name="file_attachment" id="form_attachment_input"></li>
									</ul>
								</div>
								<div class="col-sm-1"></div>
							</div>

							<div class="form-group">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label for="form_concept_input">Proof of Concepts</label>
									<ul class="list-group interactive-list" id="concepts" style="position: relative; margin-bottom: 0px;">
										<li class="list-group-item input"><input type="file" name="file_concept" id="form_concept_input"></li>
									</ul>
									<p class="help-block">Status: <span class="text-danger" id="form_review_status">Unapproved</span></p>
								</div>
								<div class="col-sm-1"></div>
							</div>

							<div class="form-group">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<a class="btn btn-default" id="request_review"><i class="glyphicon glyphicon-envelope"></i> Ask User to Review Concepts</a>
								</div>
								<div class="col-sm-1"></div>
							</div>

						</div>

					</div>

					<br><hr style="margin-top: 0px;">

					<div class="row">

						<div class="col-sm-4">

							<div class="form-group" style="margin: 0px;">
								<label class="control-label">Special Instructions</label>
								<textarea class="form-control" id="form_customer_notes" style="margin-top: 20px; height: 120px;" readonly></textarea>
							</div>

						</div>

						<div class="col-sm-4">

							<div class="form-group" style="margin: 0px;">
								<label class="control-label">Notes about Order</label>
								<textarea class="form-control" id="form_internal_notes" style="margin-top: 20px; height: 120px;"></textarea>
							</div>

						</div>

						<div class="col-sm-4">

							<div class="form-group" style="margin: 0px;">
								<label class="control-label">Notes about Customer</label>
								<textarea class="form-control" id="form_customer_global_notes" style="margin-top: 20px; height: 120px;"></textarea>
							</div>

						</div>

					</div>

				</form>

			</div>

			<div class="modal-footer" id="form_actions">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal"></span>Cancel</button>
				<button type="button" class="btn btn-success" data-action="save"></span>Save</button>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-body" style="padding-top: 20px;">

				<form class="form-horizontal" role="form">

					<table id="cart_list" class="table table-striped items">
						<thead>
							<tr>
								<th></th>
								<th style="width: 90px;">Quantity</th>
								<th>Garment</th>
								<th>Color</th>
								<th style="width: 128px;">Decoration</th>
								<th style="width: 170px;">Artwork</th>
								<th style="width: 80px;"></th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<th></th>
								<th>
									<div class="form-group" style="margin: 0px;">
										<input type="text" class="form-control" id="form_quantity" placeholder="#">
									</div>
								</th>
								<th>
									<div class="form-group" style="margin: 0px;">
										<input type="text" class="form-control" id="form_garment" placeholder="Garment Name">
									</div>
								</th>
								<th>
									<div class="form-group" style="margin: 0px;">
										<input type="text" class="form-control color-picker" id="form_color" placeholder="Color">
									</div>
								</th>
								<th>
									<div class="form-group" style="margin: 0px;">
										<select class="form-control" id="form_decoration">
											<option value="">- Select -</option>
											<option value="screen-printing">Screen Printing</option>
											<option value="embroidery">Embroidery</option>
											<option value="vinyl">Vinyl</option>
										</select>
									</div>
								</th>
								<th>
									<div class="form-group" style="margin: 0px;">
										<select class="form-control" id="form_artwork">
											<option value="">- Select -</option>
											<option value="provided">Provided by Customer</option>
											<option value="in-house">Created In-House</option>
										</select>
									</div>
								</th>
								<th><a href="#" data-action="add_item" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i> Add</a></th>
							</tr>
						</tfoot>
					</table>

				</form>

			</div>

			<div class="modal-footer" id="form_actions">
				<button type="button" class="btn btn-success" data-action="cart_return"></span>Return to Order</button>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<input type="hidden" name="id" value="">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Order?</h4>
			</div>

			<div class="modal-body">

				Label

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger" data-action="confirm_delete">Delete</button>
			</div>

		</div>
	</div>
</div>

<script defer src="elements/scripts/orders.js"></script>