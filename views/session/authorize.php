<div class="container">
	<div class="row">

		<div class="col-md-4">

		</div>

		<div class="col-md-4">

			<div class="hidden-sm hidden-xs" style="height: 100px;"></div>

			<div class="panel panel-default">
				<div class="panel-body">

					<form action="session/authorize/<?=$redirect?>" method="post" role="form">

						<div class="form-group<?=(in_array("email", $errors) ? " has-error" : "")?>">
							<input type="email" class="form-control" name="email" id="email" value="<?=(isset($_POST["email"]) ? $_POST["email"] : "")?>" placeholder="Email">
						</div>

						<div class="form-group<?=(in_array("password", $errors) ? " has-error" : "")?>">
							<input type="password" class="form-control" name="password" id="password" placeholder="Password">
						</div>

						<button type="submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-ok"></i> Sign In</button>

					</form>

				</div>
			</div>

		</div>

		<div class="col-md-4">

		</div>

	</div>
</div>