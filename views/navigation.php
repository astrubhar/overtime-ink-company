<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">

		<div class="navbar-header">

			<? if (isset($_SESSION["id"]) and $_SESSION["id"] > 0): ?>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#expanded_navigation">
				<span class="sr-only">Show Navigation Options</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<? endif ?>

			<a class="navbar-brand" href="."><img src="elements/images/brand.png" height="40" width="62" style="margin-top: -6px;"></a>

		</div>

		<? if (isset($_SESSION["id"]) and $_SESSION["id"] > 0): ?>
		<div class="collapse navbar-collapse" id="expanded_navigation">

			<ul class="nav navbar-nav">
				<li<? if ($selected == "users"): ?> class="active"<? endif ?>><a href="users"><i class="glyphicon glyphicon-user"></i> <?=($_SESSION["role"] == "administrator" ? "Users" : "Customers")?></a></li>
				<li<? if ($selected == "orders"): ?> class="active"<? endif ?>><a href="orders"><i class="glyphicon glyphicon-shopping-cart"></i> Orders</a></li>
				<li<? if ($selected == "calendar"): ?> class="active"<? endif ?>><a href="calendar"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
			</ul>

			<a href="session/destroy" class="btn btn-primary navbar-btn navbar-right"><i class="glyphicon glyphicon-off"></i></a>

			<? if (!is_null($find)): ?>
			<form class="navbar-form navbar-right hidden-sm" role="search" action="<?=$selected?>/index" method="post" style="margin-right: 0px;">

				<div class="form-group" style="max-width: 300px;">
					<div class="input-group">

						<input type="text" class="form-control" name="find" value="<?=$find?>" style="border-top-left-radius: 5px; border-bottom-left-radius: 5px;" placeholder="Search...">

						<span class="input-group-btn">
							<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
						</span>

					</div>
				</div>

			</form>
			<? endif ?>

		</div>
		<? endif ?>

	</div>
</nav>

