<!DOCTYPE html>

<html>

<head>

	<title>OverTime Ink &#187; <?=$title?></title>
	<base href="<?=$path?>" target="_self">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

	<link href="elements/styles/bootstrap.css" rel="stylesheet" media="screen">
	<link href="elements/styles/bootstrap-theme.css" rel="stylesheet" media="screen">
	<link href="elements/styles/desktop.css" rel="stylesheet" media="screen">
	<link href="elements/styles/calendar.css" rel="stylesheet" media="screen">
	<link href="elements/styles/datetimepicker.css" rel="stylesheet" media="screen">
	<link href="elements/styles/colorpicker.css" rel="stylesheet" media="screen">

	<script defer src="elements/scripts/jquery.js"></script>
	<script defer src="elements/scripts/jquery.ui.js"></script>
	<script defer src="elements/scripts/jquery.bootstrap.js"></script>
	<script defer src="elements/scripts/jquery.highlight.js"></script>
	<script defer src="elements/scripts/underscore.js"></script>
	<script defer src="elements/scripts/timezone.js"></script>
	<script defer src="elements/scripts/moment.js"></script>
	<script defer src="elements/scripts/jquery.calendar.js"></script>
	<script defer src="elements/scripts/jquery.datetimepicker.js"></script>
	<script defer src="elements/scripts/tinycolor.js"></script>
	<script defer src="elements/scripts/colorpicker.js"></script>
	<script defer src="elements/scripts/upload.js"></script>
	<script defer src="elements/scripts/desktop.js"></script>

</head>

<body>

