OverTime Ink

----------------------------------------------------------------------

Review proof of concepts for Order #<?=$id?>!

----------------------------------------------------------------------

Please see the digital proof and either approve or inform us on any
issues. You can review these online by clicking:

<?php echo $path ?>orders/review/<?php echo $id ?>/<?php echo $checksum ?>

If you have any questions or concerns, you can contact us at:

orders@overtimeink.com


Thank you,
OverTime Ink