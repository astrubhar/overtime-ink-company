<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Your shipping date for Order #<?=$id?>!</title>
</head>

<body style="background-color: #3E4651;">

	<br>

	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td width="10%"></td>
			<td width="80%" style="background-color: #FFFFFF; border-radius: 8px; padding: 20px;">

				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td style="padding-bottom: 20px; border-bottom: 1px solid #D4D4D4; font: 11pt helvetica, arial, sans-serif;">

							<img src="<?php echo $path ?>elements/images/email-logo.png" height="100" width="155">

						</td>
					</tr>
					<tr>
						<td style="padding: 40px 0px; font: 11pt helvetica, arial, sans-serif;">

							<h1 style="color: #4C91CD;  font-size: 20pt;">Your shipping date for Order #<?=$id?>!</h1>

							<p>We have approved the shipping date for your order:</p>

							<p><b><?php echo $date ?></b></p>

							<p>If you have any questions or concerns, you can contact us at <a href="mailto:orders@overtimeink.com" style="color: #014EA5;">orders@overtimeink.com</a>.

							<p><br>Thank you,<br>OverTime Ink</p>


						</td>
					</tr>
				</table>

			</td>
			<td width="10%"></td>
		</tr>
	</table>

</body>

</html>