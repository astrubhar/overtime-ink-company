OverTime Ink

----------------------------------------------------------------------

Your shipping date for Order #<?=$id?>!

----------------------------------------------------------------------

We have approved the shipping date for your order:

<?php echo $date ?>

If you have any questions or concerns, you can contact us at:

orders@overtimeink.com


Thank you,
OverTime Ink